files = [
      "conv_common_gw.vhd",
      "conv_common_gw_pkg.vhd"
    ]

modules = {
      "local" : [
          "../ip_cores/general-cores",
          "../modules"
      ]
    }
