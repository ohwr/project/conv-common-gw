--------------------------------------------------------------------------------
-- CERN (BE-CO-HT)
-- Converter board common gateware package
-- URL https://www.ohwr.org/projects/conv-common-gw/wiki/wiki
--------------------------------------------------------------------------------
--
-- Converter common gateware package
-- Description: Package for conv_common_gw entity
--
--------------------------------------------------------------------------------
-- Copyright (c) 2018 CERN
--------------------------------------------------------------------------------
-- GNU LESSER GENERAL PUBLIC LICENSE
--------------------------------------------------------------------------------
-- This source file is free software; you can redistribute it and/or modify it
-- under the terms of the GNU Lesser General Public License as published by the
-- Free Software Foundation; either version 2.1 of the License, or (at your
-- option) any later version. This source is distributed in the hope that it
-- will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
-- See the GNU Lesser General Public License for more details. You should have
-- received a copy of the GNU Lesser General Public License along with this
-- source; if not, download it from http://www.gnu.org/licenses/lgpl-2.1.html
--------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.wishbone_pkg.all;
use work.genram_pkg.all;

package conv_common_gw_pkg is

  --============================================================================
  -- Constant declarations
  --============================================================================
  -- Convenience constant if changes need be made later
  --
  -- Note that if you change this constant you will make conv-common-gw
  -- _incompatible_ with BLO and RS-485 pulse repeaters and you will need to
  -- _reimplement_ the conv_regs module
  constant c_MAX_NR_CHANS : natural := 6;
  -- ============================================================================
  -- Type declarations
  -- ============================================================================

  -- Array of constants for temperature model implemented for short pulse mode
  type t_temp_decre_step is array (0 to 15) of integer;

  --============================================================================
  -- Component declarations
  --============================================================================
  ------------------------------------------------------------------------------
  -- Top-level module
  ------------------------------------------------------------------------------
  component conv_common_gw is
    generic (
      -- Reduces some timeouts to speed up simulations
      g_SIMUL                 : boolean                       := false;
      -- Reset time: 50ns * 2 * (10**6) = 100 ms
      g_RST_TIME            : positive                      := 2*(10**6);
      -- Number of repeater channels
      g_NR_CHANS              : integer                       := 6;
      -- Number of inverter channels
      g_NR_INV_CHANS          : integer                       := 4;
      -- Board ID -- 4-letter ASCII string indicating the board ID
      -- see [1] for example
      g_BOARD_ID              : std_logic_vector(31 downto 0) := x"54424c4f";
      -- Gateware version
      g_GWVERS                : std_logic_vector(7 downto 0)  := x"40";
      -- Generate pulse repetition logic with fixed output pulse width
      g_PGEN_FIXED_WIDTH      : boolean                       := true;
      -- Pulse width at pulse generator output (valid with fixed output pulse width)
      g_PGEN_PWIDTH_LG        : natural range 2 to 40         := 24;
      g_PGEN_PWIDTH_SH        : natural range 2 to 40         := 5;
      -- Output pulse will be limited to period. They are given as n number of cycles
      -- For continuous mode operation max freq 4.16kHz
      g_PGEN_PPERIOD_CONT     : natural range 2 to 5000       := 4800;
      -- for LONG pulses changes maximum frequency to ~104kHz
      g_PGEN_PPERIOD_LG       : natural range 6 to 300        := 191;
      -- for SHORT pulses changes maximum frequency to ~2MHz
      g_PGEN_PPERIOD_SH       : natural range 2 to 300        := 9;
      -- Pulse generator glitch filter length in number of clk_20_i cycles
      g_PGEN_GF_LEN           : integer                       := 4;
      -- Burst-mode-specific generics:
      g_TEMP_DECRE_STEP_LG    : t_temp_decre_step             :=
                                      (0,0,0,0,0,0,0,0,5750,100,79,13,12,4,5,13);
      g_TEMP_DECRE_STEP_SH    : t_temp_decre_step             :=
                          (0,0,769,31,104,14,82,0,0,0,0,0,0,0,0,0);
      -- Single pulse temperature rise for long 1.2us pulses
      g_BURSTCTRL_1_PULSE_TEMP_RISE_LG
                              : in unsigned (19 downto 0)     := x"17700";
      -- Single pulse temperature rise for short 250ns pulses
      g_BURSTCTRL_1_PULSE_TEMP_RISE_SH
                              : in unsigned (19 downto 0)     := x"01388";
      -- Maximum temperature allowed (scaled)
      -- For both long 1.2us pulses and short 250ns
      g_BURSTCTRL_MAX_TEMP_LG_SH
                              : in unsigned (39 downto 0)     := x"02540BE400";
      -- Generate logic with pulse counters
      g_WITH_PULSE_CNT        : boolean                       := false;
      -- Generate logic with pulse timetag
      g_WITH_PULSE_TIMETAG    : boolean                       := false;
      -- Generate logic with manual trigger
      g_WITH_MAN_TRIG         : boolean                       := false;
      g_MAN_TRIG_PWIDTH       : integer                       := 24;
      -- Generate one-wire master for thermometer
      g_WITH_THERMOMETER      : boolean                       := false;
      -- Bicolor LED controller signals
      g_BICOLOR_LED_COLUMNS   : integer                       := 6;
      g_BICOLOR_LED_LINES     : integer                       := 2);
    port (
      -- Clocks
      clk_20_i                : in    std_logic;
      clk_125_p_i             : in    std_logic;
      clk_125_n_i             : in    std_logic;
      -- Reset output signal, synchronous to 20 MHz clock
      rst_n_o                 : out   std_logic;
      -- Glitch filter active-low enable signal
      gf_en_n_i               : in    std_logic;
      -- Burst mode enable signal. Mode disabled for all versions of board
      burst_en_n_i            : in    std_logic;
      -- Pulse width selection, port low means 250ns, high means 1.2us
      pulse_width_sel_n_i     : in    std_logic;
      -- Channel enable
      global_ch_oen_o         : out   std_logic;
      pulse_front_oen_o       : out   std_logic;
      pulse_rear_oen_o        : out   std_logic;
      inv_oen_o               : out   std_logic;
      -- Pulse I/O
      pulse_i                 : in    std_logic_vector(g_NR_CHANS-1 downto 0);
      pulse_front_i           : in    std_logic_vector(g_NR_CHANS-1 downto 0);
      pulse_rear_i            : in    std_logic_vector(g_NR_CHANS-1 downto 0);
      pulse_o                 : out   std_logic_vector(g_NR_CHANS-1 downto 0);
      -- Inverted pulse I/O
      inv_pulse_n_i           : in    std_logic_vector(g_NR_INV_CHANS-1 downto 0);
      inv_pulse_o             : out   std_logic_vector(g_NR_INV_CHANS-1 downto 0);
      -- Channel lEDs
      -- 26 ms active-high pulse on pulse_o rising edge
      led_pulse_o             : out   std_logic_vector(g_NR_CHANS-1 downto 0);
      -- Inverted channel lEDs
      -- 26 ms active-high pulse on pulse_o rising edge
      led_inv_pulse_o         : out   std_logic_vector(g_NR_INV_CHANS-1 downto 0);
      -- I2C interface
      scl_i                   : in    std_logic;
      scl_o                   : out   std_logic;
      scl_en_o                : out   std_logic;
      sda_i                   : in    std_logic;
      sda_o                   : out   std_logic;
      sda_en_o                : out   std_logic;
      -- I2C LED signals -- conect to a bicolor LED of choice
      -- led_i2c_o pulses four times on I2C transfer
      led_i2c_o               : out   std_logic;
      -- VME interface
      vme_sysreset_n_i        : in    std_logic;
      vme_ga_i                : in    std_logic_vector(4 downto 0);
      vme_gap_i               : in    std_logic;
      -- SPI interface to on-board flash chip
      flash_cs_n_o            : out   std_logic;
      flash_sclk_o            : out   std_logic;
      flash_mosi_o            : out   std_logic;
      flash_miso_i            : in    std_logic;
      -- PLL DACs
      -- 20 MHz VCXO control
      dac20_din_o             : out   std_logic;
      dac20_sclk_o            : out   std_logic;
      dac20_sync_n_o          : out   std_logic;
      -- 125 MHz clock generator control
      dac125_din_o            : out   std_logic;
      dac125_sclk_o           : out   std_logic;
      dac125_sync_n_o         : out   std_logic;
      -- SFP lines
      sfp_los_i               : in    std_logic;
      sfp_present_i           : in    std_logic;
      sfp_rate_select_o       : out   std_logic;
      sfp_sda_b               : inout std_logic;
      sfp_scl_b               : inout std_logic;
      sfp_tx_disable_o        : out   std_logic;
      sfp_tx_fault_i          : in    std_logic;
      -- Switch inputs (for readout from converter status register)
      sw_gp_i                 : in    std_logic_vector(7 downto 0);
      sw_other_i              : in    std_logic_vector(31 downto 0);
      -- PCB Version information
      hwvers_i                : in    std_logic_vector (5 downto 0);
      -- RTM lines
      rtmm_i                  : in    std_logic_vector(2 downto 0);
      rtmp_i                  : in    std_logic_vector(2 downto 0);
      -- TTL, INV-TTL and rear-panel channel inputs, for reflection in line status register
      line_front_i            : in    std_logic_vector(g_NR_CHANS-1 downto 0);
      line_inv_i              : in    std_logic_vector(g_NR_INV_CHANS-1 downto 0);
      line_rear_i             : in    std_logic_vector(g_NR_CHANS-1 downto 0);
      -- Fail-safe lines, detect invalid or no signal on channel input
      line_front_fs_i         : in    std_logic_vector(g_NR_CHANS-1 downto 0);
      line_inv_fs_i           : in    std_logic_vector(g_NR_INV_CHANS-1 downto 0);
      line_rear_fs_i          : in    std_logic_vector(g_NR_CHANS-1 downto 0);
      -- Thermometer line
      thermometer_b           : inout std_logic;
      -- System error LED, active-high on system error
      -- ERR bicolor LED should light red when led_syserr_o = '1'
      led_syserr_o            : out   std_logic;
      -- Bicolor LED signals
      bicolor_led_state_i     : in    std_logic_vector(2*g_BICOLOR_LED_COLUMNS*g_BICOLOR_LED_LINES-1 downto 0);
      bicolor_led_col_o       : out   std_logic_vector(g_BICOLOR_LED_COLUMNS-1 downto 0);
      bicolor_led_line_o      : out   std_logic_vector(g_BICOLOR_LED_LINES-1 downto 0);
      bicolor_led_line_oen_o  : out   std_logic_vector(g_BICOLOR_LED_LINES-1 downto 0));
  end component conv_common_gw;

  ------------------------------------------------------------------------------
  -- Fixed-width reset generator
  ------------------------------------------------------------------------------
  component conv_reset_gen is
    generic
    (
      -- Reset time in number of clk_i cycles
      g_RESET_TIME : positive := 2_000_000
    );
    port
    (
      clk_i   : in  std_logic;
      rst_i   : in  std_logic;
      rst_n_o : out std_logic
    );
  end component conv_reset_gen;

  ------------------------------------------------------------------------------
  -- Pulse counter - Used for scenarios where clocks are shorter than the time
  -- it takes to synchronise them (Normally it taked 3 clk cycles to synchronise
  -- trigger edge.)
  ------------------------------------------------------------------------------
  component fastevent_counter is
    port(
          sysclk_i   : in std_logic;
          rstcount_i  : in  std_logic;
          en_i        : in  std_logic;
          trig_i      : in  std_logic;
          count_o     : out std_logic_vector(31 downto 0);
          count_int_o : out unsigned(31 downto 0)
     );
  end component fastevent_counter;


  ------------------------------------------------------------------------------
  -- Pulse generator with optional configurable pulse width
  ------------------------------------------------------------------------------
  component conv_pulse_gen is
    generic
    (
      -- This generic enables elaboration of the fixed pulse width logic
      g_WITH_FIXED_PWIDTH : boolean;

      -- Pulse width, in number of clk_i cycles
      -- Default pulse width (20 MHz clock): 1.2 us
      -- Minimum allowable pulse width (20 MHz clock): 1 us
      -- Maximum allowable pulse width (20 MHz clock): 2 us
      g_PWIDTH : natural range 2 to 40 := 24;

          -- Pulse period in unit of clock cycles
      g_PPERIOD : natural := 5
    );
    port
    (
      -- Clock and active-low reset inputs
      clk_i         : in  std_logic;
      rst_n_i       : in  std_logic;

      -- Glitch filter enable input
      -- '1' - Glitch filter disabled (glitch-sensitive, no output jitter)
      -- '0' - Glitch filter enabled (glitch-insensitive, with output jitter)
      gf_en_n_i     : in  std_logic;

      -- Enable input, pulse generation is enabled when '1'
      en_i          : in  std_logic;

      -- Trigger input, has to be '1' to assure pulse output with delay no greater
      -- than internal gate delays.
      trig_a_i      : in  std_logic;
      trig_r_edge_p_i       : in std_logic; -- synced 1 cycle-long r edge output
      trig_f_edge_p_i       : in std_logic; -- synced 1 cycle-long f edge output


      -- Pulse error output, pulses high for one clock cycle when a pulse arrives
      -- within a pulse period
      pulse_err_p_o : out std_logic;

      -- Pulse output, active-high
      -- latency:
      -- glitch filter disabled: none
      -- glitch filter enabled: glitch filter length + 5 clk_i cycles
      pulse_o       : out std_logic;
      pulse_r_edge_p_o       : out std_logic; -- synced 1 cycle-long r edge output
      pulse_f_edge_p_o       : out std_logic
    );
  end component conv_pulse_gen;

    ------------------------------------------------------------------------------
  -- Controller for burst mode operation with configurable maximum pulse burst length and timeout
  ------------------------------------------------------------------------------

  component conv_dyn_burst_ctrl is
    generic (
    -- Short pulse width, in number of clk_i cycles
      -- Default short pulse width (20 MHz clock): 250 ns = 5 clk cycles
      g_PWIDTH : natural range 2 to 40  := 5;
    -- Thermal model constants, depend on mode selected short or long.
      g_TEMP_DECRE_STEP : t_temp_decre_step;
    -- Temperature rise resulting from with 250ns pulse
      g_1_PULSE_TEMP_RISE     : in unsigned (19 downto 0);   -- Check every "g_eval_burst_len" pulses

    -- Maximum temperature allowed (scaled)
      g_MAX_TEMP      : in unsigned (39 downto 0) := x"174876E800"
    );
    port (
      -- Clock and active-low reset inputs
      clk_i         : in  std_logic;
      rst_n_i       : in  std_logic;

    -- Enable input, pulse generation is enabled when '1'
      en_i            : in  std_logic;
    pulse_burst_i     : in std_logic;
    pulse_r_edge_p_i  : in std_logic;
    pulse_f_edge_p_i  : in std_logic;

    -- Temp_rise is output for external probing
    temp_rise_o       : out unsigned (39 downto 0);
    -- Dynamic temperature-controlled ouput pulse train.
    pulse_burst_o     : out std_logic;

    -- Burst error output, pulses high for one clock cycle when a pulse arrives
      -- within a burst rejection phase
      burst_err_p_o : out std_logic);

  end component conv_dyn_burst_ctrl;
    ------------------------------------------------------------------------------
    -- Converter board control registers
    ------------------------------------------------------------------------------
  component conv_regs is
    port (
      rst_n_i                                  : in     std_logic;
      clk_sys_i                                : in     std_logic;
      wb_adr_i                                 : in     std_logic_vector(5 downto 0);
      wb_dat_i                                 : in     std_logic_vector(31 downto 0);
      wb_dat_o                                 : out    std_logic_vector(31 downto 0);
      wb_cyc_i                                 : in     std_logic;
      wb_sel_i                                 : in     std_logic_vector(3 downto 0);
      wb_stb_i                                 : in     std_logic;
      wb_we_i                                  : in     std_logic;
      wb_ack_o                                 : out    std_logic;
      wb_stall_o                               : out    std_logic;
  -- Port for std_logic_vector field: 'ID register bits' in reg: 'BIDR'
      reg_bidr_i                               : in     std_logic_vector(31 downto 0);
  -- Port for std_logic_vector field: 'Gateware version' in reg: 'SR'
      reg_sr_gwvers_i                          : in     std_logic_vector(7 downto 0);
  -- Port for std_logic_vector field: 'Status of on-board general-purpose switches' in reg: 'SR'
      reg_sr_switches_i                        : in     std_logic_vector(7 downto 0);
  -- Port for std_logic_vector field: 'RTM detection lines cite{rtm-det}' in reg: 'SR'
      reg_sr_rtm_i                             : in     std_logic_vector(5 downto 0);
  -- Port for std_logic_vector field: 'Hardware version' in reg: 'SR'
      reg_sr_hwvers_i                          : in     std_logic_vector(5 downto 0);
  -- Port for BIT field: 'White Rabbit present' in reg: 'SR'
      reg_sr_wrpres_i                          : in     std_logic;
  -- Ports for BIT field: 'I2C communication watchdog timeout error' in reg: 'ERR'
      reg_err_i2c_wdto_o                       : out    std_logic;
      reg_err_i2c_wdto_i                       : in     std_logic;
      reg_err_i2c_wdto_load_o                  : out    std_logic;
  -- Ports for BIT field: 'I2C communication error' in reg: 'ERR'
      reg_err_i2c_err_o                        : out    std_logic;
      reg_err_i2c_err_i                        : in     std_logic;
      reg_err_i2c_err_load_o                   : out    std_logic;
  -- Port for std_logic_vector field: 'Frequency error' in reg: 'ERR'
      reg_err_flim_pmisse_o                    : out    std_logic_vector(5 downto 0);
      reg_err_flim_pmisse_i                    : in     std_logic_vector(5 downto 0);
      reg_err_flim_pmisse_load_o               : out    std_logic;
  -- Port for std_logic_vector field: 'Frequency watchdog error' in reg: 'ERR'
      reg_err_fwdg_pmisse_o                    : out    std_logic_vector(5 downto 0);
      reg_err_fwdg_pmisse_i                    : in     std_logic_vector(5 downto 0);
      reg_err_fwdg_pmisse_load_o               : out    std_logic;
  -- Ports for BIT field: 'Reset unlock bit' in reg: 'CR'
      reg_cr_rst_unlock_o                      : out    std_logic;
      reg_cr_rst_unlock_i                      : in     std_logic;
      reg_cr_rst_unlock_load_o                 : out    std_logic;
  -- Ports for BIT field: 'Reset bit - active only if RST_UNLOCK is 1' in reg: 'CR'
      reg_cr_rst_o                             : out    std_logic;
      reg_cr_rst_i                             : in     std_logic;
      reg_cr_rst_load_o                        : out    std_logic;
  -- Ports for PASS_THROUGH field: 'Manual Pulse Trigger' in reg: 'CR'
      reg_cr_mpt_o                             : out    std_logic_vector(7 downto 0);
      reg_cr_mpt_wr_o                          : out    std_logic;
  -- Port for std_logic_vector field: 'Value of front panel pulse counter' in reg: 'CH1FPPCR'
      reg_ch1fppcr_o                           : out    std_logic_vector(31 downto 0);
      reg_ch1fppcr_i                           : in     std_logic_vector(31 downto 0);
      reg_ch1fppcr_load_o                      : out    std_logic;
  -- Port for std_logic_vector field: 'Value of front panel pulse counter' in reg: 'CH2FPPCR'
      reg_ch2fppcr_o                           : out    std_logic_vector(31 downto 0);
      reg_ch2fppcr_i                           : in     std_logic_vector(31 downto 0);
      reg_ch2fppcr_load_o                      : out    std_logic;
  -- Port for std_logic_vector field: 'Value of front panel pulse counter' in reg: 'CH3FPPCR'
      reg_ch3fppcr_o                           : out    std_logic_vector(31 downto 0);
      reg_ch3fppcr_i                           : in     std_logic_vector(31 downto 0);
      reg_ch3fppcr_load_o                      : out    std_logic;
  -- Port for std_logic_vector field: 'Value of front panel pulse counter' in reg: 'CH4FPPCR'
      reg_ch4fppcr_o                           : out    std_logic_vector(31 downto 0);
      reg_ch4fppcr_i                           : in     std_logic_vector(31 downto 0);
      reg_ch4fppcr_load_o                      : out    std_logic;
  -- Port for std_logic_vector field: 'Value of front panel pulse counter' in reg: 'CH5FPPCR'
      reg_ch5fppcr_o                           : out    std_logic_vector(31 downto 0);
      reg_ch5fppcr_i                           : in     std_logic_vector(31 downto 0);
      reg_ch5fppcr_load_o                      : out    std_logic;
  -- Port for std_logic_vector field: 'Value of front panel pulse counter' in reg: 'CH6FPPCR'
      reg_ch6fppcr_o                           : out    std_logic_vector(31 downto 0);
      reg_ch6fppcr_i                           : in     std_logic_vector(31 downto 0);
      reg_ch6fppcr_load_o                      : out    std_logic;
  -- Port for std_logic_vector field: 'Rear panel pulse counter value' in reg: 'CH1RPPCR'
      reg_ch1rppcr_o                           : out    std_logic_vector(31 downto 0);
      reg_ch1rppcr_i                           : in     std_logic_vector(31 downto 0);
      reg_ch1rppcr_load_o                      : out    std_logic;
  -- Port for std_logic_vector field: 'Rear panel pulse counter value' in reg: 'CH2RPPCR'
      reg_ch2rppcr_o                           : out    std_logic_vector(31 downto 0);
      reg_ch2rppcr_i                           : in     std_logic_vector(31 downto 0);
      reg_ch2rppcr_load_o                      : out    std_logic;
  -- Port for std_logic_vector field: 'Rear panel pulse counter value' in reg: 'CH3RPPCR'
      reg_ch3rppcr_o                           : out    std_logic_vector(31 downto 0);
      reg_ch3rppcr_i                           : in     std_logic_vector(31 downto 0);
      reg_ch3rppcr_load_o                      : out    std_logic;
  -- Port for std_logic_vector field: 'Rear panel pulse counter value' in reg: 'CH4RPPCR'
      reg_ch4rppcr_o                           : out    std_logic_vector(31 downto 0);
      reg_ch4rppcr_i                           : in     std_logic_vector(31 downto 0);
      reg_ch4rppcr_load_o                      : out    std_logic;
  -- Port for std_logic_vector field: 'Rear panel pulse counter value' in reg: 'CH5RPPCR'
      reg_ch5rppcr_o                           : out    std_logic_vector(31 downto 0);
      reg_ch5rppcr_i                           : in     std_logic_vector(31 downto 0);
      reg_ch5rppcr_load_o                      : out    std_logic;
  -- Port for std_logic_vector field: 'Rear panel pulse counter value' in reg: 'CH6RPPCR'
      reg_ch6rppcr_o                           : out    std_logic_vector(31 downto 0);
      reg_ch6rppcr_i                           : in     std_logic_vector(31 downto 0);
      reg_ch6rppcr_load_o                      : out    std_logic;
  -- Port for std_logic_vector field: 'TAI seconds counter bits 31..0' in reg: 'TVLR'
      reg_tvlr_o                               : out    std_logic_vector(31 downto 0);
      reg_tvlr_i                               : in     std_logic_vector(31 downto 0);
      reg_tvlr_load_o                          : out    std_logic;
  -- Port for std_logic_vector field: 'TAI seconds counter bits 39..32' in reg: 'TVHR'
      reg_tvhr_o                               : out    std_logic_vector(7 downto 0);
      reg_tvhr_i                               : in     std_logic_vector(7 downto 0);
      reg_tvhr_load_o                          : out    std_logic;
  -- Port for std_logic_vector field: 'Channel mask' in reg: 'TBMR'
      reg_tbmr_chan_i                          : in     std_logic_vector(5 downto 0);
      reg_tb_rd_req_p_o                        : out    std_logic;
  -- Port for BIT field: 'White Rabbit present' in reg: 'TBMR'
      reg_tbmr_wrtag_i                         : in     std_logic;
  -- Port for std_logic_vector field: 'Cycles counter' in reg: 'TBCYR'
      reg_tbcyr_i                              : in     std_logic_vector(27 downto 0);
  -- Port for std_logic_vector field: 'Lower part of TAI seconds counter' in reg: 'TBTLR'
      reg_tbtlr_i                              : in     std_logic_vector(31 downto 0);
  -- Port for std_logic_vector field: 'Upper part of TAI seconds counter' in reg: 'TBTHR'
      reg_tbthr_i                              : in     std_logic_vector(7 downto 0);
  -- Port for std_logic_vector field: 'Buffer counter' in reg: 'TBCSR'
      reg_tbcsr_usedw_i                        : in     std_logic_vector(6 downto 0);
  -- Port for BIT field: 'Buffer full' in reg: 'TBCSR'
      reg_tbcsr_full_i                         : in     std_logic;
  -- Port for BIT field: 'Buffer empty' in reg: 'TBCSR'
      reg_tbcsr_empty_i                        : in     std_logic;
  -- Ports for BIT field: 'Clear tag buffer' in reg: 'TBCSR'
      reg_tbcsr_clr_o                          : out    std_logic;
      reg_tbcsr_clr_i                          : in     std_logic;
      reg_tbcsr_clr_load_o                     : out    std_logic;
  -- Port for std_logic_vector field: 'Cycles counter' in reg: 'CH1LTSCYR'
      reg_ch1ltscyr_i                          : in     std_logic_vector(27 downto 0);
  -- Port for std_logic_vector field: 'Lower part of TAI seconds counter' in reg: 'CH1LTSTLR'
      reg_ch1ltstlr_i                          : in     std_logic_vector(31 downto 0);
  -- Port for std_logic_vector field: 'Upper part of TAI seconds counter' in reg: 'CH1LTSTHR'
      reg_ch1ltsthr_tai_i                      : in     std_logic_vector(7 downto 0);
  -- Port for BIT field: 'White Rabbit present' in reg: 'CH1LTSTHR'
      reg_ch1ltsthr_wrtag_i                    : in     std_logic;
  -- Port for std_logic_vector field: 'Cycles counter' in reg: 'CH2LTSCYR'
      reg_ch2ltscyr_i                          : in     std_logic_vector(27 downto 0);
  -- Port for std_logic_vector field: 'Lower part of TAI seconds counter' in reg: 'CH2LTSTLR'
      reg_ch2ltstlr_i                          : in     std_logic_vector(31 downto 0);
  -- Port for std_logic_vector field: 'Upper part of TAI seconds counter' in reg: 'CH2LTSTHR'
      reg_ch2ltsthr_tai_i                      : in     std_logic_vector(7 downto 0);
  -- Port for BIT field: 'White Rabbit present' in reg: 'CH2LTSTHR'
      reg_ch2ltsthr_wrtag_i                    : in     std_logic;
  -- Port for std_logic_vector field: 'Cycles counter' in reg: 'CH3LTSCYR'
      reg_ch3ltscyr_i                          : in     std_logic_vector(27 downto 0);
  -- Port for std_logic_vector field: 'Lower part of TAI seconds counter' in reg: 'CH3LTSTLR'
      reg_ch3ltstlr_i                          : in     std_logic_vector(31 downto 0);
  -- Port for std_logic_vector field: 'Upper part of TAI seconds counter' in reg: 'CH3LTSTHR'
      reg_ch3ltsthr_tai_i                      : in     std_logic_vector(7 downto 0);
  -- Port for BIT field: 'White Rabbit present' in reg: 'CH3LTSTHR'
      reg_ch3ltsthr_wrtag_i                    : in     std_logic;
  -- Port for std_logic_vector field: 'Cycles counter' in reg: 'CH4LTSCYR'
      reg_ch4ltscyr_i                          : in     std_logic_vector(27 downto 0);
  -- Port for std_logic_vector field: 'Lower part of TAI seconds counter' in reg: 'CH4LTSTLR'
      reg_ch4ltstlr_i                          : in     std_logic_vector(31 downto 0);
  -- Port for std_logic_vector field: 'Upper part of TAI seconds counter' in reg: 'CH4LTSTHR'
      reg_ch4ltsthr_tai_i                      : in     std_logic_vector(7 downto 0);
  -- Port for BIT field: 'White Rabbit present' in reg: 'CH4LTSTHR'
      reg_ch4ltsthr_wrtag_i                    : in     std_logic;
  -- Port for std_logic_vector field: 'Cycles counter' in reg: 'CH5LTSCYR'
      reg_ch5ltscyr_i                          : in     std_logic_vector(27 downto 0);
  -- Port for std_logic_vector field: 'Lower part of TAI seconds counter' in reg: 'CH5LTSTLR'
      reg_ch5ltstlr_i                          : in     std_logic_vector(31 downto 0);
  -- Port for std_logic_vector field: 'Upper part of TAI seconds counter' in reg: 'CH5LTSTHR'
      reg_ch5ltsthr_tai_i                      : in     std_logic_vector(7 downto 0);
  -- Port for BIT field: 'White Rabbit present' in reg: 'CH5LTSTHR'
      reg_ch5ltsthr_wrtag_i                    : in     std_logic;
  -- Port for std_logic_vector field: 'Cycles counter' in reg: 'CH6LTSCYR'
      reg_ch6ltscyr_i                          : in     std_logic_vector(27 downto 0);
  -- Port for std_logic_vector field: 'Lower part of TAI seconds counter' in reg: 'CH6LTSTLR'
      reg_ch6ltstlr_i                          : in     std_logic_vector(31 downto 0);
  -- Port for std_logic_vector field: 'Upper part of TAI seconds counter' in reg: 'CH6LTSTHR'
      reg_ch6ltsthr_tai_i                      : in     std_logic_vector(7 downto 0);
  -- Port for BIT field: 'White Rabbit present' in reg: 'CH6LTSTHR'
      reg_ch6ltsthr_wrtag_i                    : in     std_logic;
  -- Port for std_logic_vector field: 'Front panel channel input state' in reg: 'LSR'
      reg_lsr_front_i                          : in     std_logic_vector(5 downto 0);
  -- Port for std_logic_vector field: 'Front panel INV-TTL input state' in reg: 'LSR'
      reg_lsr_frontinv_i                       : in     std_logic_vector(3 downto 0);
  -- Port for std_logic_vector field: 'Rear panel input state' in reg: 'LSR'
      reg_lsr_rear_i                           : in     std_logic_vector(5 downto 0);
  -- Port for std_logic_vector field: 'Front panel input failsafe state' in reg: 'LSR'
      reg_lsr_frontfs_i                        : in     std_logic_vector(5 downto 0);
  -- Port for std_logic_vector field: 'Front panel inverter input failsafe state' in reg: 'LSR'
      reg_lsr_frontinvfs_i                     : in     std_logic_vector(3 downto 0);
  -- Port for std_logic_vector field: 'Rear panel input failsafe state' in reg: 'LSR'
      reg_lsr_rearfs_i                         : in     std_logic_vector(5 downto 0);
  -- Port for std_logic_vector field: 'Switch state' in reg: 'OSWR'
      reg_oswr_switches_i                      : in     std_logic_vector(31 downto 0);
  -- Port for std_logic_vector field: 'LS bits of 1-wire DS18B20U thermometer ID' in reg: 'UIDLR'
      reg_uidlr_i                              : in     std_logic_vector(31 downto 0);
  -- Port for std_logic_vector field: 'MS bits of 1-wire DS18B20U thermometer ID' in reg: 'UIDHR'
      reg_uidhr_i                              : in     std_logic_vector(31 downto 0);
  -- Port for std_logic_vector field: 'TEMP' in reg: 'TEMPR'
      reg_tempr_i                              : in     std_logic_vector(15 downto 0));
  end component conv_regs;

  -- Converter board registers SDB definition
  constant c_CONV_REGS_SDB : t_sdb_device := (
    abi_class     => x"0000",              -- undocumented device
    abi_ver_major => x"01",
    abi_ver_minor => x"00",
    wbd_endian    => c_sdb_endian_big,
    wbd_width     => x"7",                 -- 8/16/32-bit port granularity
    sdb_component => (
    addr_first    => x"0000000000000000",
    addr_last     => x"00000000000000ff",
    product       => (
    vendor_id     => x"000000000000CE42",  -- CERN
    device_id     => x"3ab464e8",          -- echo "conv_regs          " | md5sum | cut -c1-8
    version       => x"00000001",
    date          => x"20140731",
    name          => "conv_regs          ")));

  ------------------------------------------------------------------------------
  -- Pulse time-tagging component
  ------------------------------------------------------------------------------
  component conv_pulse_timetag is
    generic
    (
    -- Frequency in Hz of the clk_i signal
      g_CLK_RATE : positive := 125000000;

    -- Number of repetition channels
      g_NR_CHAN  : positive := 6
    );
    port
    (
    -- Clock and active-low reset
      clk_i            : in  std_logic;
      rst_n_i          : in  std_logic;

    -- Asynchronous pulse input
      pulse_a_i        : in  std_logic_vector(g_nr_chan-1 downto 0);

    -- Time inputs from White Rabbit
      wr_tm_cycles_i   : in  std_logic_vector(27 downto 0);
      wr_tm_tai_i      : in  std_logic_vector(39 downto 0);
      wr_tm_valid_i    : in  std_logic;

    -- Timing inputs from Wishbone-mapped registers
      wb_tm_tai_l_i    : in  std_logic_vector(31 downto 0);
      wb_tm_tai_l_ld_i : in  std_logic;
      wb_tm_tai_h_i    : in  std_logic_vector( 7 downto 0);
      wb_tm_tai_h_ld_i : in  std_logic;

    -- Timing outputs
      tm_cycles_o      : out std_logic_vector(27 downto 0);
      tm_tai_o         : out std_logic_vector(39 downto 0);
      tm_wrpres_o      : out std_logic;
      chan_p_o         : out std_logic_vector(g_nr_chan-1 downto 0);

    -- Ring buffer I/O
      buf_wr_req_p_o   : out std_logic
    );
  end component conv_pulse_timetag;

  ------------------------------------------------------------------------------
  -- Ring buffer component
  -- use: buffer time stamps generated by the conv_pulse_timetag component
  ------------------------------------------------------------------------------
  component conv_ring_buf is
    generic
    (
      g_DATA_WIDTH : positive;
      g_SIZE       : positive
    );
    port
    (
      -- Clocks and reset
      clk_rd_i       : in  std_logic;
      clk_wr_i       : in  std_logic;
      rst_n_a_i      : in  std_logic;

      -- Buffer inputs
      buf_dat_i      : in  std_logic_vector(g_data_width-1 downto 0);
      buf_rd_req_i   : in  std_logic;
      buf_wr_req_i   : in  std_logic;
      buf_clr_i      : in  std_logic;

      -- Buffer outputs
      buf_dat_o      : out std_logic_vector(g_data_width-1 downto 0);
      buf_count_o    : out std_logic_vector(f_log2_size(g_size)-1 downto 0);
      buf_full_o     : out std_logic;
      buf_empty_o    : out std_logic
    );
  end component conv_ring_buf;

  ------------------------------------------------------------------------------
  -- Manual trigger component
  ------------------------------------------------------------------------------
  component conv_man_trig is
    generic
    (
      -- Number of conversion channels
      g_NR_CHAN : positive := 6;

      -- Length of pulse in clk_i cycles generated at trig_o output
      g_PWIDTH  : positive := 1
    );
    port
    (
      -- Clock, active-low inputs
      clk_i     : in  std_logic;
      rst_n_i   : in  std_logic;

      -- Control inputs from conv_regs
      reg_ld_i : in  std_logic;
      reg_i    : in  std_logic_vector(7 downto 0);

      -- Trigger output, g_pwidth long
      trig_o   : out std_logic_vector(g_nr_chan downto 1)
    );
  end component conv_man_trig;

  ------------------------------------------------------------------------------
  -- PPS trigger for one-wire master
  ------------------------------------------------------------------------------

  component wf_decr_counter is
    generic(g_COUNTER_LGTH : natural := 4);                         -- default length
    port(
    -- INPUTS
      -- nanoFIP User Interface general signal
      uclk_i            : in std_logic;                             -- 40 MHz clock

      -- Signal from the wf_reset_unit
      counter_rst_i     : in std_logic;                             -- resets counter to all '1'

      -- Signals from any unit
      counter_decr_i    : in std_logic;                             -- decrement enable
      counter_load_i    : in std_logic;                             -- load enable; loads counter to counter_top_i
      counter_top_i     : in unsigned (g_counter_lgth-1 downto 0);  -- load value


    -- OUTPUTS
      -- Signal to any unit
      counter_o         : out unsigned (g_counter_lgth-1 downto 0); -- counter
      counter_is_zero_o : out std_logic);                           -- empty counter indication

  end component wf_decr_counter;

  ------------------------------------------------------------------------------
  -- CHIPSCOPE COMPONENTS
  ------------------------------------------------------------------------------
  component chipscope_ila is
    port (
      CONTROL   : inout std_logic_vector(35 downto 0);
      CLK       : in    std_logic;
      TRIG0     : in    std_logic_vector(7 downto 0));

  end component chipscope_ila;


  component chipscope_icon is
    port (
      CONTROL0  : inout std_logic_vector(35 downto 0));
  end component chipscope_icon;

end package conv_common_gw_pkg;
