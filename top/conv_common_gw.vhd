--------------------------------------------------------------------------------
-- CERN (BE-CO-HT)
-- Converter board common gateware
-- URL https://www.ohwr.org/projects/conv-common-gw/wiki/wiki
--------------------------------------------------------------------------------
--
-- Converter board common gateware top-level file
--
-- Description:
--    This module is to be instantiated in all pulse converter board designs,
--    for example the CONV-TTL-BLO or CONV-TTL-RS485. It contains a set of
--    modules common to all these boards, that are configurable via generics to
--    accommodate for the application of each board.
--
-- Dependencies:
--    general-cores repository [1]
--
-- References:
--   [1] Board IDs for level conversion circuits
--       www.ohwr.org/projects/conv-common-gw/wiki/Board-id
--   [2] Platform-independent core collection webpage on OHWR,
--       http://www.ohwr.org/projects/general-cores/repository
--   [3] ELMA, Access to board data using SNMP and I2C
--       http://www.ohwr.org/documents/227
--
--------------------------------------------------------------------------------
-- Copyright (c) 2018 CERN
--------------------------------------------------------------------------------
-- GNU LESSER GENERAL PUBLIC LICENSE
--------------------------------------------------------------------------------
-- This source file is free software; you can redistribute it and/or modify it
-- under the terms of the GNU Lesser General Public License as published by the
-- Free Software Foundation; either version 2.1 of the License, or (at your
-- option) any later version. This source is distributed in the hope that it
-- will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
-- See the GNU Lesser General Public License for more details. You should have
-- received a copy of the GNU Lesser General Public License along with this
-- source; if not, download it from http://www.gnu.org/licenses/lgpl-2.1.html
--------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

use work.gencores_pkg.all;
use work.wishbone_pkg.all;
use work.genram_pkg.all;

use work.conv_common_gw_pkg.all;

entity conv_common_gw is
  generic (
    -- Reduces some timeouts to speed up simulations
    g_SIMUL                 : boolean                       := false;
    -- Reset time: 50ns * 2 * (10**6) = 100 ms
    g_RST_TIME              : positive                      := 2*(10**6);
    -- Number of repeater channels
    g_NR_CHANS              : integer                       := 6;
    -- Number of inverter channels
    g_NR_INV_CHANS          : integer                       := 4;
    -- Board ID -- 4-letter ASCII string indicating the board ID
    -- see [1] for example
    g_BOARD_ID              : std_logic_vector(31 downto 0) := x"54424c4f";
    -- Gateware version
    g_GWVERS                : std_logic_vector(7 downto 0)  := x"40";
    -- Generate pulse repetition logic with fixed output pulse width
    g_PGEN_FIXED_WIDTH      : boolean                       := true;
    -- Pulse width at pulse generator output (valid with fixed output pulse width)
    g_PGEN_PWIDTH_LG        : natural range 2 to 40         := 24;
    g_PGEN_PWIDTH_SH        : natural range 2 to 40         := 5;
    -- Output pulse will be limited to period. They are given as n number of cycles
    -- For continuous mode operation max freq 4.16kHz
    g_PGEN_PPERIOD_CONT     : natural range 2 to 5000       := 4800;
    -- for LONG pulses changes maximum frequency to ~104kHz
    g_PGEN_PPERIOD_LG       : natural range 6 to 300        := 191;
    -- for SHORT pulses changes maximum frequency to ~2MHz
    g_PGEN_PPERIOD_SH       : natural range 2 to 300        := 9;
    -- Pulse generator glitch filter length in number of clk_20_i cycles
    g_PGEN_GF_LEN           : integer                       := 4;
    -- Burst-mode-specific generics:
    g_TEMP_DECRE_STEP_LG    : t_temp_decre_step             :=
                                    (0,0,0,0,0,0,0,0,5750,100,79,13,12,4,5,13);
    g_TEMP_DECRE_STEP_SH    : t_temp_decre_step             :=
                        (0,0,769,31,104,14,82,0,0,0,0,0,0,0,0,0);
    -- Single pulse temperature rise for long 1.2us pulses
    g_BURSTCTRL_1_PULSE_TEMP_RISE_LG
                            : unsigned (19 downto 0)        := x"17700";
    -- Single pulse temperature rise for short 250ns pulses
    g_BURSTCTRL_1_PULSE_TEMP_RISE_SH
                            : unsigned (19 downto 0)        := x"01388";
    -- Maximum temperature allowed (scaled)
    -- For both long 1.2us pulses and short 250ns
    g_BURSTCTRL_MAX_TEMP_LG_SH
                            : unsigned (39 downto 0)        := x"02540BE400";
    -- Generate logic with pulse counters
    g_WITH_PULSE_CNT        : boolean                       := false;
    -- Generate logic with pulse timetag
    g_WITH_PULSE_TIMETAG    : boolean                       := false;
    -- Generate logic with manual trigger
    g_WITH_MAN_TRIG         : boolean                       := false;
    g_MAN_TRIG_PWIDTH       : integer                       := 24;
    -- Generate one-wire master for thermometer
    g_WITH_THERMOMETER      : boolean                       := false;
    -- Bicolor LED controller signals
    g_BICOLOR_LED_COLUMNS   : integer                       := 6;
    g_BICOLOR_LED_LINES     : integer                       := 2);
  port (
    -- Clocks
    clk_20_i                : in    std_logic;
    clk_125_p_i             : in    std_logic;
    clk_125_n_i             : in    std_logic;
    -- Reset output signal, synchronous to 20 MHz clock
    rst_n_o                 : out   std_logic;
    -- Glitch filter active-low enable signal
    gf_en_n_i               : in    std_logic;
    -- Burst mode enable signal. Mode disabled for all versions of board
    burst_en_n_i            : in    std_logic;
    -- Pulse width selection, port low means 250ns, high means 1.2us
    pulse_width_sel_n_i     : in    std_logic;
    -- Channel enable
    global_ch_oen_o         : out   std_logic;
    pulse_front_oen_o       : out   std_logic;
    pulse_rear_oen_o        : out   std_logic;
    inv_oen_o               : out   std_logic;
    -- Pulse I/O
    pulse_i                 : in    std_logic_vector(g_NR_CHANS-1 downto 0);
    pulse_front_i           : in    std_logic_vector(g_NR_CHANS-1 downto 0);
    pulse_rear_i            : in    std_logic_vector(g_NR_CHANS-1 downto 0);
    pulse_o                 : out   std_logic_vector(g_NR_CHANS-1 downto 0);
    -- Inverted pulse I/O
    inv_pulse_n_i           : in    std_logic_vector(g_NR_INV_CHANS-1 downto 0);
    inv_pulse_o             : out   std_logic_vector(g_NR_INV_CHANS-1 downto 0);
    -- Channel lEDs
    -- 26 ms active-high pulse on pulse_o rising edge
    led_pulse_o             : out   std_logic_vector(g_NR_CHANS-1 downto 0);
    -- Inverted channel lEDs
    -- 26 ms active-high pulse on pulse_o rising edge
    led_inv_pulse_o         : out   std_logic_vector(g_NR_INV_CHANS-1 downto 0);
    -- I2C interface
    scl_i                   : in    std_logic;
    scl_o                   : out   std_logic;
    scl_en_o                : out   std_logic;
    sda_i                   : in    std_logic;
    sda_o                   : out   std_logic;
    sda_en_o                : out   std_logic;
    -- I2C LED signals -- conect to a bicolor LED of choice
    -- led_i2c_o pulses four times on I2C transfer
    led_i2c_o               : out   std_logic;
    -- VME interface
    vme_sysreset_n_i        : in    std_logic;
    vme_ga_i                : in    std_logic_vector(4 downto 0);
    vme_gap_i               : in    std_logic;
    -- SPI interface to on-board flash chip
    flash_cs_n_o            : out   std_logic;
    flash_sclk_o            : out   std_logic;
    flash_mosi_o            : out   std_logic;
    flash_miso_i            : in    std_logic;
    -- PLL DACs
    -- 20 MHz VCXO control
    dac20_din_o             : out   std_logic;
    dac20_sclk_o            : out   std_logic;
    dac20_sync_n_o          : out   std_logic;
    -- 125 MHz clock generator control
    dac125_din_o            : out   std_logic;
    dac125_sclk_o           : out   std_logic;
    dac125_sync_n_o         : out   std_logic;
    -- SFP lines
    sfp_los_i               : in    std_logic;
    sfp_present_i           : in    std_logic;
    sfp_rate_select_o       : out   std_logic;
    sfp_sda_b               : inout std_logic;
    sfp_scl_b               : inout std_logic;
    sfp_tx_disable_o        : out   std_logic;
    sfp_tx_fault_i          : in    std_logic;
    -- Switch inputs (for readout from converter status register)
    sw_gp_i                 : in    std_logic_vector(7 downto 0);
    sw_other_i              : in    std_logic_vector(31 downto 0);
    -- PCB Version information
    hwvers_i                : in    std_logic_vector (5 downto 0);
    -- RTM lines
    rtmm_i                  : in    std_logic_vector(2 downto 0);
    rtmp_i                  : in    std_logic_vector(2 downto 0);
    -- TTL, INV-TTL and rear-panel channel inputs, for reflection in line status register
    line_front_i            : in    std_logic_vector(g_NR_CHANS-1 downto 0);
    line_inv_i              : in    std_logic_vector(g_NR_INV_CHANS-1 downto 0);
    line_rear_i             : in    std_logic_vector(g_NR_CHANS-1 downto 0);
    -- Fail-safe lines, detect invalid or no signal on channel input
    line_front_fs_i         : in    std_logic_vector(g_NR_CHANS-1 downto 0);
    line_inv_fs_i           : in    std_logic_vector(g_NR_INV_CHANS-1 downto 0);
    line_rear_fs_i          : in    std_logic_vector(g_NR_CHANS-1 downto 0);
    -- Thermometer line
    thermometer_b           : inout std_logic;
    -- System error LED, active-high on system error
    -- ERR bicolor LED should light red when led_syserr_o = '1'
    led_syserr_o            : out   std_logic;
    -- Bicolor LED signals
    bicolor_led_state_i     : in    std_logic_vector(2*g_BICOLOR_LED_COLUMNS*g_BICOLOR_LED_LINES-1 downto 0);
    bicolor_led_col_o       : out   std_logic_vector(g_BICOLOR_LED_COLUMNS-1 downto 0);
    bicolor_led_line_o      : out   std_logic_vector(g_BICOLOR_LED_LINES-1 downto 0);
    bicolor_led_line_oen_o  : out   std_logic_vector(g_BICOLOR_LED_LINES-1 downto 0));
end entity conv_common_gw;



architecture arch of conv_common_gw is

  --============================================================================
  -- Constant declarations
  --============================================================================
  -- Short pulse widths
  -- constant c_pgen_pwidth_sh   : natural := 5;

  -- Burst mode maximum duty cycle is 50%, i.e. divider is 2
  constant c_PGEN_DUTY_CYCLE_DIV_SH : natural := 2;

  -- Number of Wishbone masters and slaves, for wb_crossbar
  constant c_NR_MASTERS             : natural :=  1;
  constant c_NR_SLAVES              : natural :=  2;

  -- slave order definitions
  constant c_SLV_CONV_REGS          : natural := 0;
  constant c_SLV_MULTIBOOT          : natural := 1;

  -- base address definitions
  constant c_ADDR_CONV_REGS         : t_wishbone_address := x"00000000";
  constant c_ADDR_MULTIBOOT         : t_wishbone_address := x"00000100";
  constant c_ADDR_SDB               : t_wishbone_address := x"00000f00";


  -- SDB interconnect layout
  -- c_conv_regs_sdb defined in conv_common_gw_pkg.vhd
  constant c_SDB_LAYOUT             : t_sdb_record_array(c_NR_SLAVES-1 downto 0) := (
      c_SLV_CONV_REGS => f_sdb_embed_device(c_CONV_REGS_SDB, c_ADDR_CONV_REGS),
      c_SLV_MULTIBOOT => f_sdb_embed_device(c_XWB_XIL_MULTIBOOT_SDB,
                              c_ADDR_MULTIBOOT)
    );

  -- Tag bufferdata width:  40 -- TAI
  --                        28 -- cycles
  --                         1 -- WRPRES bit
  --                        xx -- channel mask for max. nr. channels
  constant c_TAGBUFF_DATA_WIDTH     : positive := 40 + 28 + 1 + c_max_nr_chans;

  --============================================================================
  -- Type declarations
  --============================================================================
  -- Max. channel count of c_max_nr_chans enforced here:
  type t_pulse_led_cnt is array(c_max_nr_chans-1 downto 0)
                            of unsigned(18 downto 0);

  type t_inv_pulse_led_cnt is array(g_NR_INV_CHANS-1 downto 0)
                            of unsigned(18 downto 0);

  type t_temp_rise_cnt is array(c_max_nr_chans-1 downto 0)
                            of unsigned(39 downto 0);
  type t_pulse_cnt is array(c_max_nr_chans-1 downto 0)
                        of unsigned(31 downto 0);
  type t_ch_pcr is array(c_max_nr_chans-1 downto 0)
                    of std_logic_vector(31 downto 0);

  -- Latest timestamp
  type t_lts_tai is array(c_max_nr_chans-1 downto 0)
                                  of std_logic_vector(39 downto 0);
  type t_lts_cycles is array(c_max_nr_chans-1 downto 0)
                                      of std_logic_vector(27 downto 0);

  --============================================================================
  -- Signal declarations
  --============================================================================
  -- Per-domain clock and reset signals
  signal clk_125                : std_logic;
  signal rst_125_n              : std_logic;
  signal rst_20_n               : std_logic;
  signal rst_20                 : std_logic;
  signal rst_ext                : std_logic;
  signal rst_time               : positive := 10;

  -- Pulse logic signals
  signal trig_a                 : std_logic_vector(g_NR_CHANS-1 downto 0);
  signal trig_synced            : std_logic_vector(g_NR_CHANS-1 downto 0);
  signal trig_degl              : std_logic_vector(g_NR_CHANS-1 downto 0);
  signal trig_chan              : std_logic_vector(g_NR_CHANS-1 downto 0);
  signal trig_chan_redge_p      : std_logic_vector(g_NR_CHANS-1 downto 0);
  signal trig_chan_fedge_p      : std_logic_vector(g_NR_CHANS-1 downto 0);
  signal trig_man               : std_logic_vector(g_NR_CHANS-1 downto 0);
  signal trig_pgen              : std_logic_vector(g_NR_CHANS-1 downto 0);
  signal burst_en_n             : std_logic;
  signal pulse_outp_cont        : std_logic_vector(g_NR_CHANS-1 downto 0);
  signal burst_outp_sh          : std_logic_vector(g_NR_CHANS-1 downto 0);
  signal pulse_r_edge_lg_p      : std_logic_vector(g_NR_CHANS-1 downto 0);
  signal pulse_f_edge_lg_p      : std_logic_vector(g_NR_CHANS-1 downto 0);
  signal pulse_r_edge_sh_p      : std_logic_vector(g_NR_CHANS-1 downto 0);
  signal pulse_f_edge_sh_p      : std_logic_vector(g_NR_CHANS-1 downto 0);
  signal pulse_outp_sh          : std_logic_vector(g_NR_CHANS-1 downto 0);
  signal burst_outp_lg          : std_logic_vector(g_NR_CHANS-1 downto 0);
  signal pulse_outp_lg          : std_logic_vector(g_NR_CHANS-1 downto 0);
  signal pulse_outp             : std_logic_vector(g_NR_CHANS-1 downto 0);

  signal temp_rise_c_lg         : t_temp_rise_cnt;
  signal temp_rise_c_sh         : t_temp_rise_cnt;
  signal pulse_outp_d0          : std_logic_vector(g_NR_CHANS-1 downto 0);
  signal pulse_outp_redge_p     : std_logic_vector(g_NR_CHANS-1 downto 0);
  signal inv_pulse_outp         : std_logic_vector(g_NR_INV_CHANS-1 downto 0);
  signal inv_pulse_outp_d0      : std_logic_vector(g_NR_INV_CHANS-1 downto 0);
  signal inv_pulse_outp_fedge_p : std_logic_vector(g_NR_INV_CHANS-1 downto 0);
  signal pmisse_p               : std_logic_vector(g_NR_CHANS-1 downto 0);
  signal flim_pmisse_p          : std_logic_vector(g_NR_CHANS-1 downto 0);
  signal fwdg_pmisse_p          : std_logic_vector(g_NR_CHANS-1 downto 0);
  signal pulse_outp_err_cont    : std_logic_vector(g_NR_CHANS-1 downto 0);
  signal pulse_outp_err_lg_p    : std_logic_vector(g_NR_CHANS-1 downto 0);
  signal pulse_outp_err_sh_p    : std_logic_vector(g_NR_CHANS-1 downto 0);
  signal burst_outp_err_lg_p    : std_logic_vector(g_NR_CHANS-1 downto 0);
  signal burst_outp_err_sh_p    : std_logic_vector(g_NR_CHANS-1 downto 0);

  -- Output enable signals
  signal global_oen             : std_logic;
  signal front_oen, invttl_oen  : std_logic;
  signal rear_oen               : std_logic;

  -- I2C bridge signals
  signal i2c_addr               : std_logic_vector(6 downto 0);
  signal i2c_tip                : std_logic;
  signal i2c_err_p              : std_logic;
  signal i2c_wdto_p             : std_logic;

  -- Signals to/from converter system registers component
  signal rtm_lines              : std_logic_vector(5 downto 0);
  signal sw_gp                  : std_logic_vector(7 downto 0);
  signal sw_multicast           : std_logic_vector(3 downto 0);
  signal rst_unlock             : std_logic;
  signal rst_unlock_bit         : std_logic;
  signal rst_unlock_bit_ld      : std_logic;
  signal rst_bit                : std_logic;
  signal rst_bit_ld             : std_logic;
  signal rst_fr_reg             : std_logic;
  signal i2c_wdto_bit           : std_logic;
  signal i2c_wdto_bit_rst       : std_logic;
  signal i2c_wdto_bit_rst_ld    : std_logic;
  signal pmisse_bit             : std_logic_vector(c_max_nr_chans-1 downto 0);
  signal flim_pmisse_bit        : std_logic_vector(c_max_nr_chans-1 downto 0);
  signal flim_pmisse_bit_rst    : std_logic_vector(c_max_nr_chans-1 downto 0);
  signal flim_pmisse_bit_rst_ld : std_logic;
  signal fwdg_pmisse_bit        : std_logic_vector(c_max_nr_chans-1 downto 0);
  signal fwdg_pmisse_bit_rst    : std_logic_vector(c_max_nr_chans-1 downto 0);
  signal fwdg_pmisse_bit_rst_ld : std_logic;
  signal pmisse_bits_or         : std_logic;

  -- Signals for pulse counters

  signal rst_front_cnt          : std_logic_vector(c_max_nr_chans-1 downto 0);
  signal rst_rear_cnt           : std_logic_vector(c_max_nr_chans-1 downto 0);
  signal front_pulse_cnt        : t_pulse_cnt;
  signal rear_pulse_cnt         : t_pulse_cnt;
  signal front_pulse_cnt_offset : t_pulse_cnt;
  signal rear_pulse_cnt_offset  : t_pulse_cnt;
  signal front_pulse_c          : t_pulse_cnt;
  signal rear_pulse_c           : t_pulse_cnt;
  signal ch_front_pcr           : t_ch_pcr;
  signal ch_front_pcr_ld        : std_logic_vector(c_max_nr_chans-1 downto 0);
  signal ch_rear_pcr            : t_ch_pcr;
  signal ch_rear_pcr_ld         : std_logic_vector(c_max_nr_chans-1 downto 0);
  signal mpt_ld                 : std_logic;
  signal mpt                    : std_logic_vector( 7 downto 0);
  signal tvlr                   : std_logic_vector(31 downto 0);
  signal tvlr_ld                : std_logic;
  signal tvhr                   : std_logic_vector( 7 downto 0);
  signal tvhr_ld                : std_logic;
  signal wrpres                 : std_logic;
  signal i2c_err_bit            : std_logic;
  signal i2c_err_bit_rst        : std_logic;
  signal i2c_err_bit_rst_ld     : std_logic;
  signal line_front             : std_logic_vector(c_max_nr_chans-1 downto 0);
  signal line_rear              : std_logic_vector(c_max_nr_chans-1 downto 0);
  signal line_front_fs          : std_logic_vector(c_max_nr_chans-1 downto 0);
  signal line_rear_fs           : std_logic_vector(c_max_nr_chans-1 downto 0);

  -- LED signals
  signal led_pulse              : std_logic_vector(g_NR_CHANS-1 downto 0);
  signal led_pulse_cnt          : t_pulse_led_cnt;
  signal led_inv_pulse          : std_logic_vector(g_NR_INV_CHANS-1 downto 0);
  signal led_inv_pulse_cnt      : t_pulse_led_cnt;
  signal led_i2c                : std_logic;
  signal led_i2c_clkdiv         : unsigned(18 downto 0);
  signal led_i2c_cnt            : unsigned( 2 downto 0);
  signal led_i2c_blink          : std_logic;

  -- Wishbone crossbar signals
  signal xbar_slave_in          : t_wishbone_slave_in_array  (c_NR_MASTERS-1 downto 0);
  signal xbar_slave_out         : t_wishbone_slave_out_array (c_NR_MASTERS-1 downto 0);
  signal xbar_master_in         : t_wishbone_master_in_array (c_NR_SLAVES-1 downto 0);
  signal xbar_master_out        : t_wishbone_master_out_array(c_NR_SLAVES-1 downto 0);

  -- Time-tagging component signals
  signal tm_cycles              : std_logic_vector(27 downto 0);
  signal tm_tai                 : std_logic_vector(39 downto 0);

  signal buf_wr_req_p           : std_logic;
  signal buf_rd_req_p           : std_logic;
  signal buf_count              : std_logic_vector(f_log2_size(128)-1 downto 0);
  signal buf_full               : std_logic;
  signal buf_empty              : std_logic;
  signal buf_chan               : std_logic_vector(g_NR_CHANS-1 downto 0);
  signal buf_wrtag              : std_logic;
  signal buf_clr_bit            : std_logic;
  signal buf_clr_bit_ld         : std_logic;
  signal buf_clr_p              : std_logic;
  signal buf_dat_in             : std_logic_vector(c_TAGBUFF_DATA_WIDTH-1 downto 0);
  signal buf_dat_out            : std_logic_vector(c_TAGBUFF_DATA_WIDTH-1 downto 0);

  -- Latest timestamp signals
  signal lts_ld_125             : std_logic_vector(c_max_nr_chans-1 downto 0);
  signal lts_ld_rdy_125         : std_logic_vector(c_max_nr_chans-1 downto 0);
  signal lts_tai_125            : t_lts_tai;
  signal lts_cycles_125         : t_lts_cycles;
  signal lts_wrtag_125          : std_logic_vector(c_max_nr_chans-1 downto 0);
  signal lts_ld_toggle          : std_logic_vector(c_max_nr_chans-1 downto 0);
  signal lts_ld_toggle_d0       : std_logic_vector(c_max_nr_chans-1 downto 0);
  signal lts_ld_toggle_d1       : std_logic_vector(c_max_nr_chans-1 downto 0);
  signal lts_ld_20              : std_logic_vector(c_max_nr_chans-1 downto 0);
  signal lts_tai_20             : t_lts_tai;
  signal lts_cycles_20          : t_lts_cycles;
  signal lts_wrtag_20           : std_logic_vector(c_max_nr_chans-1 downto 0);

  -- One-wire master signals
  signal owr_en                 : std_logic_vector(0 downto 0);
  signal owr_in                 : std_logic_vector(0 downto 0);

  signal pps_is_zero            : std_logic;
  signal tmp_id                 : std_logic_vector(63 downto 0);
  signal tmp_temper             : std_logic_vector(15 downto 0);
  signal onewire_read_p         : std_logic;

  signal pps_load_p             : std_logic;
  signal id                     : std_logic_vector(63 downto 0);
  signal temper                 : std_logic_vector(15 downto 0);

  -- Chipscope signals
  ---------------------------------------------------------------------------------------------------
  signal CONTROL                : std_logic_vector(35 downto 0);
  signal CLK                    : std_logic;
  signal TRIG0_in               : std_logic_vector(7 downto 0);

   -- signal TRIG1_in           : std_logic_vector(7 downto 0);
   -- signal TRIG2_in           : std_logic_vector(7 downto 0);
   -- signal TRIG3_in           : std_logic_vector(7 downto 0);
   -- signal TRIG4_in           : std_logic_vector(7 downto 0);
   -- signal TRIG5_in           : std_logic_vector(7 downto 0);
   -- signal TRIG6_in           : std_logic_vector(7 downto 0);
   -- signal TRIG7_in           : std_logic_vector(7 downto 0);
   -- signal TRIG8_in           : std_logic_vector(7 downto 0);
   -- signal TRIG9_in           : std_logic_vector(7 downto 0);
   -- signal TRIG10_in          : std_logic_vector(7 downto 0);
   -- signal TRIG11_in          : std_logic_vector(7 downto 0);

--==============================================================================
--  architecture begin
--==============================================================================
begin

---------------------------------------------------------------------------------------------------
--       CHIPSCOPE       --
---------------------------------------------------------------------------------------------------
  -- chipscope_ila_1 : chipscope_ila
  -- port map (
    -- CONTROL => CONTROL,
    -- CLK     => clk_20_i,
    -- TRIG0   => TRIG0_in);

  -- chipscope_icon_1 : chipscope_icon
  -- port map ( CONTROL0 => CONTROL);


  -- TRIG0_in(0)   <= pulse_outp_err_sh_p(5);
  -- TRIG0_in(1)   <= burst_outp_err_sh_p(5);

  -- TRIG0_in(2)   <= trig_pgen(5);
  -- TRIG0_in(3)   <= pulse_outp_sh(5);

  -- TRIG0_in(4)   <= pulse_outp_err_lg_p(5);
  -- TRIG0_in(5)   <= burst_outp_err_lg_p(5);
  -- TRIG0_in(6)   <= pulse_outp_lg(5);
  -- TRIG0_in(7)   <= ch_front_pcr_ld(5);


  --============================================================================
  -- Differential input buffer for 125 MHz clock
  --============================================================================
  cmp_clk_125_buf : IBUFGDS
    generic map (
      DIFF_TERM    => true,  -- Differential Termination
      IBUF_LOW_PWR => true,  -- Low power (TRUE) vs. performance (FALSE) setting
                             -- for referenced I/O standards
      IOSTANDARD   => "DEFAULT")
    port map (
      O  => clk_125,
      I  => clk_125_p_i,
      IB => clk_125_n_i
      );

  --============================================================================
  -- Internal and external reset generation
  --============================================================================
  -- External reset input to reset generator
  rst_ext <= rst_fr_reg or (not vme_sysreset_n_i);



  -- Configure reset generator for 100ms reset
  cmp_reset_gen : conv_reset_gen
    generic map
    (
      g_RESET_TIME => g_RST_TIME
    )
    port map
    (
      clk_i   => clk_20_i,
      rst_i   => rst_ext,
      rst_n_o => rst_20_n
    );

  -- Output reset signal is synchronous to the 20 MHz clock
  rst_n_o <= rst_20_n;

  -- And synchronize the 20 MHz domain reset into the 125 MHz domain
  cmp_sync_rst : gc_sync_ffs
   generic map
   (
     g_sync_edge => "positive"
   )
   port map
   (
     clk_i    => clk_125,
     rst_n_i  => '1',
     data_i   => rst_20_n,
     synced_o => rst_125_n
   );

  --============================================================================
  -- Output enable logic
  --============================================================================
  -- Enable outputs only when the FPGA is ready to handle them
  -- One clock cycle delay from global OEN to rest of OENs

  p_delay_oen : process (clk_20_i) is
  begin
    if rising_edge(clk_20_i) then
      if rst_20_n = '0' then
        global_oen <= '0';
        front_oen    <= '0';
        invttl_oen <= '0';
        rear_oen   <= '0';
        burst_en_n <= '0';
      else
        global_oen <= '1';
        if global_oen = '1' then
          front_oen    <= '1';
          invttl_oen <= '1';
          rear_oen   <= '1';
          burst_en_n <= burst_en_n_i;
        end if;
      end if;
    end if;
  end process p_delay_oen;

  -- Assign OEN outputs
  global_ch_oen_o   <= global_oen;
  pulse_front_oen_o <= front_oen;
  inv_oen_o         <= invttl_oen;
  pulse_rear_oen_o  <= rear_oen;

  --============================================================================
  -- Pulse repetition logic
  --============================================================================
  trig_a <= pulse_i;

--------------------------------------------------------------------------------
  gen_man_trig : if g_WITH_MAN_TRIG = true generate
  -- Manual trigger logic
    cmp_man_trig : conv_man_trig
    generic map
    (
      g_nr_chan => g_NR_CHANS,
      g_pwidth  => g_MAN_TRIG_PWIDTH
    )
    port map
    (
      clk_i    => clk_20_i,
      rst_n_i  => rst_20_n,
      reg_ld_i => mpt_ld,
      reg_i    => mpt,
      trig_o   => trig_man
    );
  end generate gen_man_trig;
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
  gen_no_man_trig : if g_WITH_MAN_TRIG = false generate
    trig_man <= (others => '0');
  end generate gen_no_man_trig;
--------------------------------------------------------------------------------
-- Glitch filter
--------------------------------------------------------------------------------

  gen_pulse_chan_logic : for i in 0 to g_NR_CHANS-1 generate

  -- Synchronize the asynchronous trigger input into the 20 MHz clock
  -- domain before passing it to the glitch filter
    cmp_trig_sync : gc_sync_ffs
    generic map
    (
      g_sync_edge => "positive"
    )
    port map
    (
      clk_i    => clk_20_i,
      rst_n_i  => rst_20_n,
      data_i   => trig_a(i),
      synced_o => trig_synced(i)
    );

  -- Deglitch synchronized trigger signal
    cmp_inp_glitch_filt : gc_glitch_filt
    generic map
    (
      g_len => g_PGEN_GF_LEN
    )
    port map
    (
      clk_i   => clk_20_i,
      rst_n_i => rst_20_n,
      dat_i   => trig_synced(i),
      dat_o   => trig_degl(i)
    );

  -- Now that we have a deglitched signal, generate the MUX to select between
  -- deglitched and direct channel input
    trig_chan(i) <= trig_a(i) when gf_en_n_i = '1' else
                  trig_degl(i);


  -- The trigger to the pulse generator is either manual OR from the channel input
    trig_pgen(i) <= trig_chan(i) or trig_man(i);

  -- Now, sync this channel trigger signal before passing it to the counters
  --
  -- The pulse counter is triggered only by a pulse that actually makes it
  -- to the pulse generator.
  --
  -- NOTE: glitch-filtered signal is also synced in 20MHz clock domain, but
  -- another sync chain here avoids extra logic complication and should have
  -- no influence on the correctness of the pulse counter value

    cmp_sync_ffs : gc_sync_ffs
    port map
    (
      clk_i    => clk_20_i,
      rst_n_i  => rst_20_n,
      data_i   => trig_pgen(i),
      ppulse_o => trig_chan_redge_p(i),
      npulse_o => trig_chan_fedge_p(i)
    );


--------------------------------------------------------------------------------
-- Pulse counters:
--------------------------------------------------------------------------------
-- Use Flacter based fast counters to count fast pulses
-- See for more details https://www.doulos.com/knowhow/fpga/fastcounter/
-- The counter below has additional integer output value of the counter plus a
-- reset signal that depends on the system reset in addition to the counter load pulse
--------------------------------------------------------------------------------
    gen_pulse_cnt : if g_WITH_PULSE_CNT = true generate
      rst_front_cnt(i) <= rst_20 or ch_front_pcr_ld(i);
      rst_rear_cnt(i) <= rst_20 or ch_rear_pcr_ld(i);

      cmp_pulse_cnt_front : fastevent_counter
      port map(
            sysclk_i    => clk_20_i,
            rstcount_i  => rst_front_cnt(i),
            en_i        => '1',
            trig_i      => pulse_front_i(i),
            count_int_o => front_pulse_c(i));

      cmp_pulse_cnt_rear :  fastevent_counter
      port map(
            sysclk_i    => clk_20_i,
            rstcount_i  => rst_rear_cnt(i),
            en_i        => '1',
            trig_i      => pulse_rear_i(i),
            count_int_o => rear_pulse_c(i));

  -- First, the pulse counters for the used channels (up to g_NR_CHANS)

      p_pulse_cnt : process (clk_20_i)
      begin
        if rising_edge(clk_20_i) then
          if rst_20_n = '0' then
            front_pulse_cnt(i)    <= (others => '0');
            rear_pulse_cnt(i)    <= (others => '0');
            front_pulse_cnt_offset(i) <= (others => '0');
            rear_pulse_cnt_offset(i) <= (others => '0');
          elsif ch_front_pcr_ld(i) = '1' then
            front_pulse_cnt_offset(i) <= unsigned(ch_front_pcr(i));
          elsif ch_rear_pcr_ld(i) = '1' then
            rear_pulse_cnt_offset(i) <= unsigned(ch_rear_pcr(i));
          else
            front_pulse_cnt(i) <= front_pulse_cnt_offset(i)+ front_pulse_c(i);
            rear_pulse_cnt(i) <= rear_pulse_cnt_offset(i)+ rear_pulse_c(i);
          end if;
        end if;
      end process p_pulse_cnt;


--------------------------------------------------------------------------------
-- Connect pulse counter values for unused channels to all zeroes
      gen_pulse_cnt_unused_chans : if g_NR_CHANS < c_max_nr_chans generate
        front_pulse_cnt(c_max_nr_chans-1 downto g_NR_CHANS) <= (others => (others => '0'));
        rear_pulse_cnt(c_max_nr_chans-1 downto g_NR_CHANS) <= (others => (others => '0'));

      end generate gen_pulse_cnt_unused_chans;
--------------------------------------------------------------------------------
    end generate gen_pulse_cnt;
--
-------------------------------------------------------------------------------------------------
-- Instantiate pulse generator alone for CONTINUOUS MODE
-------------------
-- Instantiate pulse generator block for continuous operation without burst feature

    cmp_pulse_gen_cont : conv_pulse_gen
    generic map
    (
      g_with_fixed_pwidth => g_PGEN_FIXED_WIDTH,
      g_pwidth            => g_PGEN_PWIDTH_LG,
      g_pperiod           => g_PGEN_PPERIOD_CONT
    )
    port map
    (
      clk_i               => clk_20_i,
      rst_n_i             => rst_20_n,
      gf_en_n_i           => gf_en_n_i,
      en_i                => '1',
      trig_a_i            => trig_pgen(i),
      trig_r_edge_p_i     => trig_chan_redge_p(i),
      trig_f_edge_p_i     => trig_chan_fedge_p(i),
      pulse_err_p_o       => pulse_outp_err_cont (i),
      pulse_o             => pulse_outp_cont(i)
    );

  ----------------------------------------------------------------------------------------------
  -- Instantiate pulse generator + burst controller block for the channel for LONG pulse operation
  -------------------------------------------------------------------------------------------------
  -- BURST MODE WITH LONG PULSES
  ----------------------------------
  -- Instantiate pulse generator block for minimum pulse width and minimum allowed duty cycle
    cmp_pulse_gen_lg : conv_pulse_gen
    generic map
    (
      g_with_fixed_pwidth => g_PGEN_FIXED_WIDTH,
      g_pwidth            => g_PGEN_PWIDTH_LG,
      g_pperiod           => g_PGEN_PPERIOD_LG
    )
    port map
    (
      clk_i               => clk_20_i,
      rst_n_i             => rst_20_n,
      gf_en_n_i           => gf_en_n_i,
      en_i                => '1',
      trig_a_i            => trig_pgen(i),
      trig_r_edge_p_i     => trig_chan_redge_p(i),
      trig_f_edge_p_i     => trig_chan_fedge_p(i),
      pulse_err_p_o       => pulse_outp_err_lg_p (i),
      pulse_o             => pulse_outp_lg(i),
      pulse_r_edge_p_o    => pulse_r_edge_lg_p(i),
      pulse_f_edge_p_o    => pulse_f_edge_lg_p(i)
    );
    ----------------------------------------------------------------------------------
    -- Instantiate burst control block for the channel
    cmp_burst_ctrl_lg :  conv_dyn_burst_ctrl
    generic map
    (
        g_pwidth                    => g_PGEN_PWIDTH_LG,
        g_temp_decre_step           => g_TEMP_DECRE_STEP_LG,
        g_1_pulse_temp_rise         => g_BURSTCTRL_1_PULSE_TEMP_RISE_LG,
        g_max_temp                  => g_BURSTCTRL_MAX_TEMP_LG_SH
    )
    port map
    (
        clk_i                       => clk_20_i,
        rst_n_i                     => rst_20_n,
        en_i                        => '1',
        pulse_burst_i               => pulse_outp_lg(i),
        pulse_r_edge_p_i            => pulse_r_edge_lg_p(i),
        pulse_f_edge_p_i            => pulse_f_edge_lg_p(i),
        temp_rise_o                 => temp_rise_c_lg(i),
        pulse_burst_o               => burst_outp_lg(i),
        burst_err_p_o               => burst_outp_err_lg_p(i)
    );

     ----------------------------------------------------------------------------------------------
  -- Instantiate pulse generator + burst controller block for the channel for SHORT pulse operation
  -------------------------------------------------------------------------------------------------
  -- BURST MODE WITH SHORT PULSES
  ----------------------------------
  -- Instantiate pulse generator block for minimum pulse width and minimum allowed duty cycle
    cmp_pulse_gen_sh : conv_pulse_gen
    generic map(
      g_with_fixed_pwidth => g_PGEN_FIXED_WIDTH,
      g_pwidth            => g_PGEN_PWIDTH_SH,
      g_pperiod           => g_PGEN_PPERIOD_SH
    )
    port map(
      clk_i               => clk_20_i,
      rst_n_i             => rst_20_n,
      gf_en_n_i           => gf_en_n_i,
      en_i                => '1',
      trig_a_i            => trig_pgen(i),
      trig_r_edge_p_i     => trig_chan_redge_p(i),
      trig_f_edge_p_i     => trig_chan_fedge_p(i),
      pulse_err_p_o       => pulse_outp_err_sh_p(i),
      pulse_o             => pulse_outp_sh(i),
      pulse_r_edge_p_o    => pulse_r_edge_sh_p(i),
      pulse_f_edge_p_o    => pulse_f_edge_sh_p(i));
    ----------------------------------------------------------------------------------
    -- Instantiate burst control block for the channel
    cmp_burst_ctrl_sh :  conv_dyn_burst_ctrl
    generic map(
      g_pwidth            => g_PGEN_PWIDTH_SH,
      g_temp_decre_step   => g_TEMP_DECRE_STEP_SH,
      g_1_pulse_temp_rise => g_BURSTCTRL_1_PULSE_TEMP_RISE_SH,
      g_max_temp          => g_BURSTCTRL_MAX_TEMP_LG_SH)
    port map(
        clk_i               => clk_20_i,
        rst_n_i             => rst_20_n,
        en_i                => '1',
        pulse_burst_i       => pulse_outp_sh(i),
        pulse_r_edge_p_i    => pulse_r_edge_sh_p(i),
        pulse_f_edge_p_i    => pulse_f_edge_sh_p(i),
        temp_rise_o         => temp_rise_c_sh(i),
        pulse_burst_o       => burst_outp_sh(i),
        burst_err_p_o       => burst_outp_err_sh_p(i)
    );

    ----------------------------------------------------------------------
    -- Select output depending on mode of operation.
    ----------------------------------------------------------------------

    pulse_outp (i) <= (burst_outp_lg(i) and pulse_width_sel_n_i) or
                    (burst_outp_sh(i) and not pulse_width_sel_n_i)
                    when burst_en_n = '0'
                    else pulse_outp_cont(i);
    ----------------------------------------------------------------------
    -- Generate error pulses depending on mode of operation
    ----------------------------------------------------------------------
    -- flim_pmisse_p gives out a pulse when a pulse is missed because its
    -- frequency is above the set maximum frequency
    flim_pmisse_p (i) <= (pulse_outp_err_lg_p(i) and pulse_width_sel_n_i) or
                    (pulse_outp_err_sh_p(i) and not pulse_width_sel_n_i)
                    when burst_en_n = '0'
                    else pulse_outp_err_cont(i);

    -- fwdg_pmisse_p gives out a pulse when a pulse is cutoff because the
    -- frequency watchdog only supports a high frequency for a limited period
    fwdg_pmisse_p (i) <= (burst_outp_err_lg_p(i) and pulse_width_sel_n_i) or
                    (burst_outp_err_sh_p(i) and not pulse_width_sel_n_i)
                    when burst_en_n = '0'
                    else '0';
    pmisse_p (i)    <= flim_pmisse_p (i) or fwdg_pmisse_p (i);

    -----------------------------------------------------------------------
  -- Process to flash pulse LED when a pulse is output
  -- LED flash length: 26 ms
    p_pulse_led : process (clk_20_i) is
    begin
      if rising_edge(clk_20_i) then
        if rst_20_n = '0' then
          pulse_outp_d0(i)      <= '0';
          pulse_outp_redge_p(i) <= '0';
          led_pulse_cnt(i)      <= (others => '0');
          led_pulse(i)          <= '0';
        else
          pulse_outp_d0(i)      <= pulse_outp(i);
          pulse_outp_redge_p(i) <= pulse_outp(i) and (not pulse_outp_d0(i));
          case led_pulse(i) is
            when '0' =>
              if pulse_outp_redge_p(i) = '1' then
                led_pulse(i) <= '1';
              end if;
            when '1' =>
              led_pulse_cnt(i) <= led_pulse_cnt(i) + 1;
              if led_pulse_cnt(i) = (led_pulse_cnt(i)'range => '1') then
                led_pulse(i) <= '0';
              end if;
            when others =>
              led_pulse(i) <= '0';
          end case;
        end if;
      end if;
    end process p_pulse_led;

  end generate gen_pulse_chan_logic;


-- Process to flash INV-TTL LEDs on the falling edge of the INV-TTL input
-- LED flash length: 26 ms

  gen_inv_ttl_leds : for i in 0 to g_NR_INV_CHANS-1 generate

-- INV-TTL outputs
    inv_pulse_outp(i) <= inv_pulse_n_i(i);
    inv_pulse_o(i) <= inv_pulse_outp(i);

    p_inv_pulse_led : process (clk_20_i) is
    begin
      if rising_edge(clk_20_i) then
        if rst_20_n = '0' then
          inv_pulse_outp_d0(i)      <= '0';
          inv_pulse_outp_fedge_p(i) <= '0';
          led_inv_pulse_cnt(i)   <= (others => '0');
          led_inv_pulse(i)       <= '0';
        else
          inv_pulse_outp_d0(i)      <= inv_pulse_outp(i);
          inv_pulse_outp_fedge_p(i) <= (not inv_pulse_outp(i)) and inv_pulse_outp_d0(i);
          case led_inv_pulse(i) is
            when '0' =>
              if inv_pulse_outp_fedge_p(i) = '1' then
                led_inv_pulse(i) <= '1';
              end if;
            when '1' =>
              led_inv_pulse_cnt(i) <= led_inv_pulse_cnt(i) + 1;
              if led_inv_pulse_cnt(i) = (led_inv_pulse_cnt(i)'range => '1') then
                led_inv_pulse(i) <= '0';
              end if;
            when others =>
              led_inv_pulse(i) <= '0';
          end case;
        end if;
      end if;
    end process p_inv_pulse_led;
  end generate gen_inv_ttl_leds;


--------------------------------------------------------------------------------
  gen_pulse_timetag : if g_WITH_PULSE_TIMETAG = true generate
    cmp_pulse_timetag : conv_pulse_timetag
      generic map (
        -- Frequency in Hz of the clk_i signal
        g_clk_rate       => 125000000,
        -- Number of repetition channels
        g_nr_chan        => g_NR_CHANS)
      port map(
    -- Clock and active-low reset
        clk_i            => clk_125,
        rst_n_i          => rst_125_n,
    -- Asynchronous pulse input
        pulse_a_i        => trig_chan,
    -- Time inputs from White Rabbit
        wr_tm_cycles_i   => (others => '0'),
        wr_tm_tai_i      => (others => '0'),
        wr_tm_valid_i    => '0',
    -- Timing inputs from Wishbone-mapped registers
        wb_tm_tai_l_i    => tvlr,
        wb_tm_tai_l_ld_i => tvlr_ld,
        wb_tm_tai_h_i    => tvhr,
        wb_tm_tai_h_ld_i => tvhr_ld,
    -- Timing outputs
        tm_cycles_o      => tm_cycles,
        tm_tai_o         => tm_tai,
        tm_wrpres_o      => buf_wrtag,
        chan_p_o         => buf_chan,
    -- Ring buffer I/O
        buf_wr_req_p_o   => buf_wr_req_p
    );

--------------------------------------------------------------------------------
    gen_buf_chan : if g_NR_CHANS = c_max_nr_chans generate
      buf_dat_in(c_max_nr_chans-1 downto  0) <= buf_chan;
    end generate gen_buf_chan;

    gen_buf_chan_unused_chans : if g_NR_CHANS < c_max_nr_chans generate
      buf_dat_in(g_NR_CHANS-1 downto 0) <= buf_chan;
      buf_dat_in(c_max_nr_chans-1 downto g_NR_CHANS) <= (others => '0');
    end generate gen_buf_chan_unused_chans;
--------------------------------------------------------------------------------

    buf_dat_in( 6)           <= buf_wrtag;
    buf_dat_in(34 downto  7) <= tm_cycles;
    buf_dat_in(74 downto 35) <= tm_tai;

  -- Instantiate the ring buffer
    cmp_ring_buf : conv_ring_buf
      generic map (
      g_data_width => c_TAGBUFF_DATA_WIDTH,
      g_size       => 128)
      port map (
      -- Clocks and reset
        clk_rd_i       => clk_20_i,
        clk_wr_i       => clk_125,
        rst_n_a_i      => rst_20_n,

      -- Buffer inputs
        buf_dat_i      => buf_dat_in,
        buf_rd_req_i   => buf_rd_req_p,
        buf_wr_req_i   => buf_wr_req_p,
        buf_clr_i      => buf_clr_p,

      -- Buffer outputs
        buf_dat_o      => buf_dat_out,
        buf_full_o     => buf_full,
        buf_empty_o    => buf_empty,
        buf_count_o    => buf_count);
  end generate gen_pulse_timetag;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

  -- Channel output assignments
  pulse_o         <= pulse_outp;
  led_pulse_o     <= led_pulse;
  led_inv_pulse_o <= led_inv_pulse;

  --============================================================================
  -- I2C bridge logic
  --============================================================================
  -- Set the I2C address signal according to ELMA protocol [2]
  i2c_addr <= "10" & vme_ga_i;

  -- Instantiate I2C bridge component
  --
  -- FSM watchdog timeout timer:
  -- * consider bit period of 30 us
  -- * 10 bits / byte transfer => 300 us
  -- * 40 bytes in one transfer => 12000 us
  -- * clk_i period = 50 ns => g_fsm_wdt = 12000 us / 50 ns = 240000
  -- * multiply by two for extra safety => g_fsm_wdt = 480000
  -- * Time to watchdog timeout: 480000 * 50ns = 24 ms
  cmp_i2c_bridge : wb_i2c_bridge
    generic map
    (
      g_fsm_wdt => 480000
    )
    port map
    (
      -- Clock, reset
      clk_i      => clk_20_i,
      rst_n_i    => rst_20_n,

      -- I2C lines
      scl_i      => scl_i,
      scl_o      => scl_o,
      scl_en_o   => scl_en_o,
      sda_i      => sda_i,
      sda_o      => sda_o,
      sda_en_o   => sda_en_o,

      -- I2C address and status
      i2c_addr_i => i2c_addr,

      -- TIP and ERR outputs
      tip_o      => i2c_tip,
      err_p_o    => i2c_err_p,
      wdto_p_o   => i2c_wdto_p,

      -- Wishbone master signals
      wbm_stb_o  => xbar_slave_in(0).stb,
      wbm_cyc_o  => xbar_slave_in(0).cyc,
      wbm_sel_o  => xbar_slave_in(0).sel,
      wbm_we_o   => xbar_slave_in(0).we,
      wbm_dat_i  => xbar_slave_out(0).dat,
      wbm_dat_o  => xbar_slave_in(0).dat,
      wbm_adr_o  => xbar_slave_in(0).adr,
      wbm_ack_i  => xbar_slave_out(0).ack,
      wbm_rty_i  => xbar_slave_out(0).rty,
      wbm_err_i  => xbar_slave_out(0).err);

  -- Process to blink the LED when an I2C transfer is in progress
  -- blinks four times per transfer
  -- blink width : 20 ms
  -- blink period: 40 ms
  p_i2c_blink : process(clk_20_i)
  begin
    if rising_edge(clk_20_i) then
      if rst_20_n = '0' then
        led_i2c_clkdiv <= (others => '0');
        led_i2c_cnt    <= (others => '0');
        led_i2c        <= '0';
        led_i2c_blink  <= '0';
      else
        case led_i2c_blink is

          when '0' =>
            led_i2c <= '0';
            if i2c_tip = '1' then
              led_i2c_blink <= '1';
            end if;

          when '1' =>
            led_i2c_clkdiv <= led_i2c_clkdiv + 1;
            if led_i2c_clkdiv = 399999 then
              led_i2c_clkdiv <= (others => '0');
              led_i2c_cnt    <= led_i2c_cnt + 1;
              led_i2c        <= not led_i2c;
              if led_i2c_cnt = 7 then
                led_i2c_cnt <= (others => '0');
                led_i2c_blink <= '0';
              end if;
            end if;

          when others =>
            led_i2c_blink <= '0';

        end case;
      end if;
    end if;
  end process p_i2c_blink;

  -- And set the I2C LED Output
  led_i2c_o <= led_i2c;

  -- Register for the I2C_WDTO bit in the SR, cleared by writing a '1'
  p_sr_wdto_bit : process(clk_20_i)
  begin
    if rising_edge(clk_20_i) then
      if rst_20_n = '0' then
        i2c_wdto_bit <= '0';
      elsif i2c_wdto_p = '1' then
        i2c_wdto_bit <= '1';
      elsif (i2c_wdto_bit_rst_ld = '1') and (i2c_wdto_bit_rst = '1') then
        i2c_wdto_bit <= '0';
      end if;
    end if;
  end process p_sr_wdto_bit;

  -- Register for the I2C_ERR bit in the SR
  p_i2c_err_led : process(clk_20_i)
  begin
    if rising_edge(clk_20_i) then
      if rst_20_n = '0' then
        i2c_err_bit <= '0';
      elsif i2c_err_p = '1' then
        i2c_err_bit <= '1';
      elsif (i2c_err_bit_rst_ld = '1') and (i2c_err_bit_rst = '1') then
        i2c_err_bit <= '0';
      end if;
    end if;
  end process p_i2c_err_led;

  --============================================================================
  -- Instantiation and connection of the main Wishbone crossbar
  --============================================================================
  cmp_wb_crossbar : xwb_sdb_crossbar
    generic map
    (
      g_num_masters => c_NR_MASTERS,
      g_num_slaves  => c_NR_SLAVES,
      g_registered  => false,
      g_wraparound  => true,
      g_layout      => c_SDB_LAYOUT,
      g_sdb_addr    => c_ADDR_SDB
    )
    port map
    (
      clk_sys_i => clk_20_i,
      rst_n_i   => rst_20_n,
      slave_i   => xbar_slave_in,
      slave_o   => xbar_slave_out,
      master_i  => xbar_master_in,
      master_o  => xbar_master_out
    );

  --============================================================================
  -- Converter board registers
  --============================================================================
  -- RTM lines combo
  rtm_lines    <= rtmp_i & rtmm_i;

--------------------------------------------------------------------------------
  gen_line : if g_NR_CHANS = c_max_nr_chans generate
    line_front    <= line_front_i;
    line_rear     <= line_rear_i;
    line_front_fs <= line_front_fs_i;
    line_rear_fs  <= line_rear_fs_i;
  end generate gen_line;
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
  gen_line_unused_chans : if g_NR_CHANS < c_max_nr_chans generate
  -- connect used lines
    line_front(g_NR_CHANS-1 downto 0)    <= line_front_i;
    line_rear(g_NR_CHANS-1 downto 0)     <= line_rear_i;
    line_front_fs(g_NR_CHANS-1 downto 0) <= line_front_fs_i;
    line_rear_fs(g_NR_CHANS-1 downto 0)  <= line_rear_fs_i;

  -- unused lines to zeroes
    line_front(c_max_nr_chans-1 downto g_NR_CHANS)    <= (others => '0');
    line_rear(c_max_nr_chans-1 downto g_NR_CHANS)     <= (others => '0');
    line_front_fs(c_max_nr_chans-1 downto g_NR_CHANS) <= (others => '0');
    line_rear_fs(c_max_nr_chans-1 downto g_NR_CHANS)  <= (others => '0');
  end generate gen_line_unused_chans;
--------------------------------------------------------------------------------

  -- Implement the RST_UNLOCK bit
  p_rst_unlock : process (clk_20_i)
  begin
    if rising_edge(clk_20_i) then
      if rst_20_n = '0' then
        rst_unlock <= '0';
      elsif rst_unlock_bit_ld = '1' then
        if rst_unlock_bit = '1' then
          rst_unlock <= '1';
        else
          rst_unlock <= '0';
        end if;
      end if;
    end if;
  end process p_rst_unlock;

  -- Implement the reset bit register
  -- The register can only be set when the RST_UNLOCK bit is '1'.
  p_rst_fr_reg : process (clk_20_i)
  begin
    if rising_edge(clk_20_i) then
      if rst_20_n = '0' then
        rst_fr_reg <= '0';
      elsif (rst_bit_ld = '1') and (rst_bit = '1') and (rst_unlock = '1') then
        rst_fr_reg <= '1';
      else
        rst_fr_reg <= '0';
      end if;
    end if;
  end process p_rst_fr_reg;

  -- Register for the PMISSE bits in the ERR, set when a channel misses a pulse
  -- Each bit is cleared by writing a '1' to it
  p_err_pmisse_bit : process (clk_20_i)
  begin
    if rising_edge(clk_20_i) then
      for i in 0 to g_NR_CHANS-1 loop
        if rst_20_n = '0' then
          flim_pmisse_bit(i) <= '0';
          fwdg_pmisse_bit (i) <= '0';
          pmisse_bit(i) <= '0';
        elsif pmisse_p(i) = '1' then
          if flim_pmisse_p (i) = '1' then
            flim_pmisse_bit(i) <= '1';
          end if;
          if fwdg_pmisse_p (i) = '1' then
            fwdg_pmisse_bit(i) <= '1';
          end if;
        else
          if (flim_pmisse_bit_rst_ld = '1') and (flim_pmisse_bit_rst(i) = '1') then
            flim_pmisse_bit(i) <= '0';
          end if;
          if (fwdg_pmisse_bit_rst_ld = '1') and (fwdg_pmisse_bit_rst(i) = '1') then
            fwdg_pmisse_bit(i) <= '0';
          end if;
        end if;
        pmisse_bit(i) <= flim_pmisse_bit(i) or fwdg_pmisse_bit(i);
      end loop;
    end if;
  end process p_err_pmisse_bit;


  -- Create an OR of all PMISSE bits
  pmisse_bits_or <= '0' when pmisse_bit = (pmisse_bit'range => '0') else
                    '1';

--------------------------------------------------------------------------------
-- Set the rest of the PMISSE bits to zero when g_NR_CHANS < c_max_nr_chans
  gen_pmisse_unused_chans : if g_NR_CHANS < c_max_nr_chans generate
    pmisse_bit(c_max_nr_chans-1 downto g_NR_CHANS) <= (others => '0');
  end generate gen_pmisse_unused_chans;
--------------------------------------------------------------------------------

  -- Synchronize WR valid signal to implement the WRPRES bit
  cmp_wrpres_sync : gc_sync_ffs
    generic map
    (
      g_sync_edge => "positive")
    port map
    (
      clk_i    => clk_20_i,
      rst_n_i  => rst_20_n,
      data_i   => buf_wrtag,
      synced_o => wrpres);

  -- Implement the TBCSR.CLR bit
  p_tbcsr_clr : process (clk_20_i)
  begin
    if rising_edge(clk_20_i) then
      if rst_20_n = '0' then
        buf_clr_p <= '0';
      else
        buf_clr_p <= '0';
        if (buf_clr_bit_ld = '1') and (buf_clr_bit = '1') then
          buf_clr_p <= '1';
        end if;
      end if;
    end if;
  end process p_tbcsr_clr;

  -- Implement the latest timestamp registers
  -- NOTE: Updated in the 125 MHz clock domain
  lts_ld_125 <= buf_chan;

  p_lts_125 : process (clk_125)
  begin
    if rising_edge(clk_125) then
      for i in 0 to g_NR_CHANS-1 loop
        if rst_125_n = '0' then
          lts_cycles_125(i) <= (others => '0');
          lts_tai_125(i)    <= (others => '0');
          lts_wrtag_125(i)  <= '0';
        -- load only when synchronized version in 20 MHz domain is ready
        elsif (lts_ld_125(i) = '1') and (lts_ld_rdy_125(i) = '1') then
          lts_cycles_125(i) <= tm_cycles;
          lts_tai_125(i)    <= tm_tai;
          lts_wrtag_125(i)  <= buf_wrtag;
        end if;
      end loop;
    end if;
  end process p_lts_125;

  -- Pulse synchronizer: sync. lts_ld from 125 MHz to 20MHz domain
  gen_lts_ld_pulse_sync : for i in 0 to g_NR_CHANS-1 generate
    cmp_pulse_sync : gc_pulse_synchronizer2
      port map(
        clk_in_i    => clk_125,
        rst_in_n_i  => rst_125_n,

        clk_out_i   => clk_20_i,
        rst_out_n_i => rst_20_n,

        d_ready_o   => lts_ld_rdy_125(i),

        d_p_i       => lts_ld_125(i),
        q_p_o       => lts_ld_20(i));
  end generate gen_lts_ld_pulse_sync;

  -- Latest timestamp regs in 20MHz clock domain
  p_lts_20 : process (clk_20_i)
  begin
    if rising_edge(clk_20_i) then
      for i in 0 to g_NR_CHANS-1 loop
        if rst_20_n = '0' then
          lts_cycles_20(i) <= (others => '0');
          lts_tai_20(i)    <= (others => '0');
          lts_wrtag_20(i)  <= '0';
        elsif lts_ld_20(i) = '1' then
          lts_cycles_20(i) <= lts_cycles_125(i);
          lts_tai_20(i)    <= lts_tai_125(i);
          lts_wrtag_20(i)  <= lts_wrtag_125(i);
        end if;
      end loop;
    end if;
  end process p_lts_20;

--------------------------------------------------------------------------------
  -- Connect unused timestamps to all zeroes
  gen_latest_timestamp_unused_chans : if g_NR_CHANS < c_max_nr_chans generate
    lts_ld_125(c_max_nr_chans-1 downto g_NR_CHANS)     <= (others => '0');
    lts_ld_rdy_125(c_max_nr_chans-1 downto g_NR_CHANS) <= (others => '0');
    lts_cycles_125(c_max_nr_chans-1 downto g_NR_CHANS) <= (others => (others => '0'));
    lts_tai_125(c_max_nr_chans-1 downto g_NR_CHANS)    <= (others => (others => '0'));
    lts_wrtag_125(c_max_nr_chans-1 downto g_NR_CHANS)  <= (others => '0');
    lts_ld_20(c_max_nr_chans-1 downto g_NR_CHANS)      <= (others => '0');
    lts_cycles_20(c_max_nr_chans-1 downto g_NR_CHANS)  <= (others => (others => '0'));
    lts_tai_20(c_max_nr_chans-1 downto g_NR_CHANS)     <= (others => (others => '0'));
    lts_wrtag_20(c_max_nr_chans-1 downto g_NR_CHANS)   <= (others => '0');
  end generate gen_latest_timestamp_unused_chans;
--------------------------------------------------------------------------------

  -- Then, instantiate the component
  cmp_conv_regs : conv_regs
    port map (
      rst_n_i                        => rst_20_n,
      clk_sys_i                      => clk_20_i,

      wb_adr_i                       => xbar_master_out(c_SLV_CONV_REGS).adr(7 downto 2),
      wb_dat_i                       => xbar_master_out(c_SLV_CONV_REGS).dat,
      wb_dat_o                       => xbar_master_in (c_SLV_CONV_REGS).dat,
      wb_cyc_i                       => xbar_master_out(c_SLV_CONV_REGS).cyc,
      wb_sel_i                       => xbar_master_out(c_SLV_CONV_REGS).sel,
      wb_stb_i                       => xbar_master_out(c_SLV_CONV_REGS).stb,
      wb_we_i                        => xbar_master_out(c_SLV_CONV_REGS).we,
      wb_ack_o                       => xbar_master_in (c_SLV_CONV_REGS).ack,
      wb_stall_o                     => xbar_master_in (c_SLV_CONV_REGS).stall,

      reg_bidr_i                     => g_BOARD_ID,

      reg_sr_gwvers_i                => g_GWVERS,
      reg_sr_switches_i              => sw_gp_i,
      reg_sr_rtm_i                   => rtm_lines,
      reg_sr_hwvers_i                => hwvers_i,
      reg_sr_wrpres_i                => wrpres,

      reg_err_i2c_wdto_o             => i2c_wdto_bit_rst,
      reg_err_i2c_wdto_i             => i2c_wdto_bit,
      reg_err_i2c_wdto_load_o        => i2c_wdto_bit_rst_ld,
      reg_err_i2c_err_o              => i2c_err_bit_rst,
      reg_err_i2c_err_i              => i2c_err_bit,
      reg_err_i2c_err_load_o         => i2c_err_bit_rst_ld,
      reg_err_flim_pmisse_o          => flim_pmisse_bit_rst,
      reg_err_flim_pmisse_i          => flim_pmisse_bit,
      reg_err_flim_pmisse_load_o     => flim_pmisse_bit_rst_ld,
      reg_err_fwdg_pmisse_o          => fwdg_pmisse_bit_rst,
      reg_err_fwdg_pmisse_i          => fwdg_pmisse_bit,
      reg_err_fwdg_pmisse_load_o     => fwdg_pmisse_bit_rst_ld,

      reg_cr_rst_unlock_o            => rst_unlock_bit,
      reg_cr_rst_unlock_i            => rst_unlock,
      reg_cr_rst_unlock_load_o       => rst_unlock_bit_ld,
      reg_cr_rst_o                   => rst_bit,
      reg_cr_rst_i                   => rst_fr_reg,
      reg_cr_rst_load_o              => rst_bit_ld,
      reg_cr_mpt_o                   => mpt,
      reg_cr_mpt_wr_o                => mpt_ld,

      reg_ch1fppcr_o                 => ch_front_pcr(0),
      reg_ch1fppcr_i                 => std_logic_vector(front_pulse_cnt(0)),
      reg_ch1fppcr_load_o            => ch_front_pcr_ld(0),
      reg_ch2fppcr_o                 => ch_front_pcr(1),
      reg_ch2fppcr_i                 => std_logic_vector(front_pulse_cnt(1)),
      reg_ch2fppcr_load_o            => ch_front_pcr_ld(1),
      reg_ch3fppcr_o                 => ch_front_pcr(2),
      reg_ch3fppcr_i                 => std_logic_vector(front_pulse_cnt(2)),
      reg_ch3fppcr_load_o            => ch_front_pcr_ld(2),
      reg_ch4fppcr_o                 => ch_front_pcr(3),
      reg_ch4fppcr_i                 => std_logic_vector(front_pulse_cnt(3)),
      reg_ch4fppcr_load_o            => ch_front_pcr_ld(3),
      reg_ch5fppcr_o                 => ch_front_pcr(4),
      reg_ch5fppcr_i                 => std_logic_vector(front_pulse_cnt(4)),
      reg_ch5fppcr_load_o            => ch_front_pcr_ld(4),
      reg_ch6fppcr_o                 => ch_front_pcr(5),
      reg_ch6fppcr_i                 => std_logic_vector(front_pulse_cnt(5)),
      reg_ch6fppcr_load_o            => ch_front_pcr_ld(5),

      reg_ch1rppcr_o                 => ch_rear_pcr(0),
      reg_ch1rppcr_i                 => std_logic_vector(rear_pulse_cnt(0)),
      reg_ch1rppcr_load_o            => ch_rear_pcr_ld(0),
      reg_ch2rppcr_o                 => ch_rear_pcr(1),
      reg_ch2rppcr_i                 => std_logic_vector(rear_pulse_cnt(1)),
      reg_ch2rppcr_load_o            => ch_rear_pcr_ld(1),
      reg_ch3rppcr_o                 => ch_rear_pcr(2),
      reg_ch3rppcr_i                 => std_logic_vector(rear_pulse_cnt(2)),
      reg_ch3rppcr_load_o            => ch_rear_pcr_ld(2),
      reg_ch4rppcr_o                 => ch_rear_pcr(3),
      reg_ch4rppcr_i                 => std_logic_vector(rear_pulse_cnt(3)),
      reg_ch4rppcr_load_o            => ch_rear_pcr_ld(3),
      reg_ch5rppcr_o                 => ch_rear_pcr(4),
      reg_ch5rppcr_i                 => std_logic_vector(rear_pulse_cnt(4)),
      reg_ch5rppcr_load_o            => ch_rear_pcr_ld(4),
      reg_ch6rppcr_o                 => ch_rear_pcr(5),
      reg_ch6rppcr_i                 => std_logic_vector(rear_pulse_cnt(5)),
      reg_ch6rppcr_load_o            => ch_rear_pcr_ld(5),

      reg_tvlr_o                     => tvlr,
      reg_tvlr_i                     => tm_tai(31 downto 0),
      reg_tvlr_load_o                => tvlr_ld,
      reg_tvhr_o                     => tvhr,
      reg_tvhr_i                     => tm_tai(39 downto 32),
      reg_tvhr_load_o                => tvhr_ld,

      reg_tbmr_chan_i                => buf_dat_out( 5 downto  0),
      reg_tbmr_wrtag_i               => buf_dat_out( 6),
      reg_tb_rd_req_p_o              => buf_rd_req_p,
      reg_tbcyr_i                    => buf_dat_out(34 downto  7),
      reg_tbtlr_i                    => buf_dat_out(66 downto 35),
      reg_tbthr_i                    => buf_dat_out(74 downto 67),
      reg_tbcsr_clr_o                => buf_clr_bit,
      reg_tbcsr_clr_i                => '0',
      reg_tbcsr_clr_load_o           => buf_clr_bit_ld,
      reg_tbcsr_usedw_i              => buf_count,
      reg_tbcsr_full_i               => buf_full,
      reg_tbcsr_empty_i              => buf_empty,

      reg_ch1ltscyr_i                => lts_cycles_20(0),
      reg_ch1ltstlr_i                => lts_tai_20(0)(31 downto  0),
      reg_ch1ltsthr_tai_i            => lts_tai_20(0)(39 downto 32),
      reg_ch1ltsthr_wrtag_i          => lts_wrtag_20(0),
      reg_ch2ltscyr_i                => lts_cycles_20(1),
      reg_ch2ltstlr_i                => lts_tai_20(1)(31 downto  0),
      reg_ch2ltsthr_tai_i            => lts_tai_20(1)(39 downto 32),
      reg_ch2ltsthr_wrtag_i          => lts_wrtag_20(1),
      reg_ch3ltscyr_i                => lts_cycles_20(2),
      reg_ch3ltstlr_i                => lts_tai_20(2)(31 downto  0),
      reg_ch3ltsthr_tai_i            => lts_tai_20(2)(39 downto 32),
      reg_ch3ltsthr_wrtag_i          => lts_wrtag_20(2),
      reg_ch4ltscyr_i                => lts_cycles_20(3),
      reg_ch4ltstlr_i                => lts_tai_20(3)(31 downto  0),
      reg_ch4ltsthr_tai_i            => lts_tai_20(3)(39 downto 32),
      reg_ch4ltsthr_wrtag_i          => lts_wrtag_20(3),
      reg_ch5ltscyr_i                => lts_cycles_20(4),
      reg_ch5ltstlr_i                => lts_tai_20(4)(31 downto  0),
      reg_ch5ltsthr_tai_i            => lts_tai_20(4)(39 downto 32),
      reg_ch5ltsthr_wrtag_i          => lts_wrtag_20(4),
      reg_ch6ltscyr_i                => lts_cycles_20(5),
      reg_ch6ltstlr_i                => lts_tai_20(5)(31 downto  0),
      reg_ch6ltsthr_tai_i            => lts_tai_20(5)(39 downto 32),
      reg_ch6ltsthr_wrtag_i          => lts_wrtag_20(5),

      reg_lsr_front_i                => line_front,
      reg_lsr_frontinv_i             => line_inv_i,
      reg_lsr_rear_i                 => line_rear,
      reg_lsr_frontfs_i              => line_front_fs,
      reg_lsr_frontinvfs_i           => line_inv_fs_i,
      reg_lsr_rearfs_i               => line_rear_fs,


      reg_oswr_switches_i            => sw_other_i,

      reg_uidlr_i                    => id (31 downto 0),
      reg_uidhr_i                    => id (63 downto 32),
      reg_tempr_i                    => temper
    );

  --============================================================================
  -- Instantiate Xilinx MultiBoot module
  --============================================================================
  cmp_multiboot : xwb_xil_multiboot
    port map
    (
      clk_i      => clk_20_i,
      rst_n_i    => rst_20_n,
      wbs_i      => xbar_master_out(c_SLV_MULTIBOOT),
      wbs_o      => xbar_master_in(c_SLV_MULTIBOOT),
      spi_cs_n_o => flash_cs_n_o,
      spi_sclk_o => flash_sclk_o,
      spi_mosi_o => flash_mosi_o,
      spi_miso_i => flash_miso_i
    );

  --============================================================================
  -- On-board DS18B20 Thermometer logic
  --============================================================================
--------------------------------------------------------------------------------
  gen_thermometer : if g_WITH_THERMOMETER = true generate
  -- The one-wire interface component is used to read-out the on-board DS18B20
  -- unique ID and temperature

    cmp_onewire : gc_ds182x_interface
    generic map (freq => 20)
    port map
      (clk_i     => clk_20_i,
       rst_n_i   => rst_20_n,
       pps_p_i   => pps_is_zero,
       onewire_b => thermometer_b,
       id_o      => tmp_id,
       temper_o  => tmp_temper,
       id_read_o => onewire_read_p,
       id_ok_o   => open);
  end generate gen_thermometer;
 --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --
 -- pps generator based on the 20 MHz clk
  cmp_pps_gen : wf_decr_counter
  generic map(
  g_counter_lgth   => 25
  )
  port map
    (uclk_i            => clk_20_i,
     counter_rst_i     => rst_20,
     counter_decr_i    => '1',
     counter_load_i    => pps_load_p,
     counter_top_i     => "1001100010010110100000000", -- 20'000'000
     counter_is_zero_o => pps_is_zero);
  --  --  --  --  --  --  --  --  --  --  --
  pps_load_p <= pps_is_zero; -- looping
  rst_20 <= not rst_20_n;
 --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --
  -- registering of the read values
  reg_reading : process(clk_20_i)
  begin
    if rising_edge(clk_20_i) then
      temper   <= (others => '0');
      id       <= (others => '0');
      if onewire_read_p = '1' then
        temper <= tmp_temper;
        id     <= tmp_id;
      end if;
    end if;
  end process reg_reading;



  -- ============================================================================
  -- Bicolor LED matrix logic
  -- ============================================================================
  -- connect column, line & state signals outside
  cmp_bicolor_led_ctrl : gc_bicolor_led_ctrl
    generic map
    (
      g_NB_COLUMN    => g_BICOLOR_LED_COLUMNS,
      g_NB_LINE      => g_BICOLOR_LED_LINES,
      g_clk_freq     => 20000000,
      g_refresh_rate => 250
    )
    port map
    (
      clk_i           => clk_20_i,
      rst_n_i         => rst_20_n,
      led_intensity_i => "1111111",
      led_state_i     => bicolor_led_state_i,
      column_o        => bicolor_led_col_o,
      line_o          => bicolor_led_line_o,
      line_oen_o      => bicolor_led_line_oen_o
    );

  -- Set the system error signal for the ERR LED
  led_syserr_o <= i2c_err_bit or i2c_wdto_bit or pmisse_bits_or;

  -- ============================================================================
  -- Drive unused outputs with safe values
  -- ============================================================================
  -- DAC outputs: enables to '1' (disable DAC comm interface) and SCK, DIN to '0'
  dac20_sync_n_o  <= '1';
  dac20_din_o     <= '0';
  dac20_sclk_o    <= '0';
  dac125_sync_n_o <= '1';
  dac125_din_o    <= '0';
  dac125_sclk_o   <= '0';

  -- SFP lines all open-drain, set to high-impedance
  sfp_rate_select_o <= 'Z';
  sfp_sda_b         <= 'Z';
  sfp_scl_b         <= 'Z';
  sfp_tx_disable_o  <= 'Z';

end architecture arch;

--==============================================================================
--  architecture end
--==============================================================================
