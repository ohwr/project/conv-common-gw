onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /testbench/clk20
add wave -noupdate /testbench/rst_n
add wave -noupdate /testbench/mpt_ld
add wave -noupdate /testbench/mpt
add wave -noupdate /testbench/trig_man
add wave -noupdate -expand /testbench/pulse
add wave -noupdate -divider DUT
add wave -noupdate /testbench/cmp_dut/state
add wave -noupdate /testbench/cmp_dut/cnt
add wave -noupdate /testbench/cmp_dut/trig_o
add wave -noupdate -divider {glitch filt}
add wave -noupdate /testbench/gen_pulse_gens(1)/cmp_pulse_gen/gf_en_n_i
add wave -noupdate /testbench/gen_pulse_gens(2)/cmp_pulse_gen/gf_en_n_i
add wave -noupdate /testbench/gen_pulse_gens(3)/cmp_pulse_gen/gf_en_n_i
add wave -noupdate /testbench/gen_pulse_gens(4)/cmp_pulse_gen/gf_en_n_i
add wave -noupdate /testbench/gen_pulse_gens(5)/cmp_pulse_gen/gf_en_n_i
add wave -noupdate /testbench/gen_pulse_gens(6)/cmp_pulse_gen/gf_en_n_i
add wave -noupdate -divider {pulse gen}
add wave -noupdate /testbench/gen_pulse_gens(1)/cmp_pulse_gen/pulse_gf_off
add wave -noupdate /testbench/gen_pulse_gens(1)/cmp_pulse_gen/pulse_gf_on
add wave -noupdate /testbench/gen_pulse_gens(2)/cmp_pulse_gen/pulse_gf_off
add wave -noupdate /testbench/gen_pulse_gens(2)/cmp_pulse_gen/pulse_gf_on
add wave -noupdate /testbench/gen_pulse_gens(3)/cmp_pulse_gen/pulse_gf_off
add wave -noupdate /testbench/gen_pulse_gens(3)/cmp_pulse_gen/pulse_gf_on
add wave -noupdate /testbench/gen_pulse_gens(4)/cmp_pulse_gen/pulse_gf_off
add wave -noupdate /testbench/gen_pulse_gens(4)/cmp_pulse_gen/pulse_gf_on
add wave -noupdate /testbench/gen_pulse_gens(5)/cmp_pulse_gen/pulse_gf_off
add wave -noupdate /testbench/gen_pulse_gens(5)/cmp_pulse_gen/pulse_gf_on
add wave -noupdate /testbench/gen_pulse_gens(6)/cmp_pulse_gen/pulse_gf_off
add wave -noupdate /testbench/gen_pulse_gens(6)/cmp_pulse_gen/pulse_gf_on
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {218986486 ps} 0} {{Cursor 2} {74966216 ps} 0}
configure wave -namecolwidth 383
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {105 us}
