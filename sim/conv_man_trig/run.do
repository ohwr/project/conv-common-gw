vlib work

vsim -t 1ps -voptargs="+acc" -lib work work.testbench

radix -hexadecimal
#add wave *
do wave.do

run 300 us
wave zoomfull
