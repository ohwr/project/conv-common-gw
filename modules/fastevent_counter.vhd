-- Q: I want to capture very fast pulses on a slower clock. the pulses are
--    shorter than a clock period but will not arrive more often than one
--    every few cycles.  How do I detect them?
-- A: This circuit.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity fastevent_counter is
  port (sysclk_i   : in std_logic;
        rstcount_i : in std_logic;
        en_i   : in std_logic;
        trig_i     : in std_logic;
        count_o    : out std_logic_vector(31 downto 0);
        count_int_o: out unsigned(31 downto 0));
end entity;

architecture rtl of fastevent_counter is

  -- flancter signals
  signal setce  : std_logic;
  signal clrce  : std_logic;
  signal flag   : std_logic;
  signal flag_sync : std_logic;
  signal setflop : std_logic := '0';
  signal clrflop : std_logic := '0';

  --count value
  -- signal count_int : unsigned(7 downto 0);
  signal rst_counter, incr_counter : std_logic;
  signal count_int : unsigned(31 downto 0);
  
  type state_t is (wait_flag, flag_set);
  signal state, nextstate: state_t := wait_flag;

begin

-- This is the flancter bit:
  -- set flop
  set_proc:process(trig_i)
  begin
    if rising_edge(trig_i) then
      if setce = '1' then
        -- flops get opposite logic levels.
        setflop <= not clrflop;
      end if;
    end if;
  end process;

  -- clr flop
  clr_proc : process(sysclk_i)
  begin
    if rising_edge(sysclk_i) then
      if clrce = '1' then
        -- flops get the same logic levels.
        clrflop <= setflop;
      end if;
    end if;
  end process;

 -- sync the flag into the sysclk_i domain.
 -- clear the sync regs in one go to react to the next trig earlier.
  flag_sync_p : process(sysclk_i) begin
    if rising_edge(sysclk_i) then
      if clrce = '1' then
        flag_sync <= '0';
        flag <= '0';
      else
        flag_sync <= setflop xor clrflop;
        flag <= flag_sync;
      end if;
    end if;
  end process;

  -- the counter
  counter_p : process (sysclk_i) begin
    if rising_edge(sysclk_i) then
      if rst_counter = '1' then
        count_int <= (others => '0');
      elsif incr_counter = '1' then
        count_int <= count_int + 1;
      end if;
    end if;
  end process;

-- Two-state machine to keep track of what we're doing
-- Either we're waiting for a pulse, or we've had one
-- and we need to react to it by incrementing the counter and reseting the flag.
  nextstate_p : process (state, en_i, flag, rstcount_i) is begin
    setce <= '0';
    clrce <= '0';
    incr_counter <= '0';
    rst_counter <= '0';
    nextstate <= state;
    case state is
      when wait_flag => -- wait for flag to be set
        if en_i = '1' then
          setce <= '1';
          if flag = '1' then
            nextstate <= flag_set;
          end if;
        end if;
        if rstcount_i = '1' then
          rst_counter <= '1';
        end if;
      when flag_set => -- flag set, so trig happened
                       -- increment count and clr flag
        incr_counter <= '1';
        clrce <= '1';
        nextstate <= wait_flag;
    end case;
  end process;

  state_p : process(sysclk_i) begin
    if rising_edge(sysclk_i) then
      state <= nextstate;
    end if;
  end process;

  count_o         <= std_logic_vector(count_int);
  count_int_o     <= count_int; 
end architecture rtl;