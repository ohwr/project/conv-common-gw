--==============================================================================
-- CERN (BE-CO-HT)
-- Pulse generator with trigger
--==============================================================================
--
-- author: 
--
-- date of creation: 2013-03-01
--
-- version: 2.0
--
-- description:
--    This module generates a constant-width pulse. The width is set using the
--    g_pwidth generic, given in number of clk_i cycles. The module in principle
--    should generate either 250ns pulses or 1.2us. With a clk_i period of
--    50 ns, the output pulse width is by default 5 or 24clock cycles long respectively.
--
--    The module is designed to work with an external glitch filter. Enabling
--    this glitch filter will result in jitter on the leading edge of the
--    output pulse signal. This jitter can be avoided by bypassing the glitch
--    filter; this is done via the gf_en_n_i input.
--
--    Regardless of whether the glitch filter is enabled, the input trigger signal
--    is extended or cut to g_pwidth, if it is shorter or respectively longer than
--    g_pwidth. At the end of the pulse, a rejection phase is implemented in order
--    to avoid too many pulses arriving on the input. This is to safeguard the
--    blocking output stage of the CONV-TTL-BLO boards. The isolation phase limits
--    the input pulse to 1/2 cycles for 250 ns pulses and 1/8 for 1.2 us pulses.
--    The burst cntrol module placed immediately after this module, will integrate 
--    pulse repetition frequency over time according to a pre-defined "thermal" model.
--
-- dependencies:
--    none
--
--==============================================================================
-- GNU LESSER GENERAL PUBLIC LICENSE
--==============================================================================
-- This source file is free software; you can redistribute it and/or modify it
-- under the terms of the GNU Lesser General Public License as published by the
-- Free Software Foundation; either version 2.1 of the License, or (at your
-- option) any later version. This source is distributed in the hope that it
-- will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
-- See the GNU Lesser General Public License for more details. You should have
-- received a copy of the GNU Lesser General Public License along with this
-- source; if not, download it from http://www.gnu.org/licenses/lgpl-2.1.html
--==============================================================================
-- last changes:
--    01-03-2013   Theodor Stana     File created.
--    02-03-2017   Denia Bouhired    Almost total re-write of the code to make for a more general FSM

--==============================================================================
-- TODO: -
--==============================================================================

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.gencores_pkg.all;

entity conv_pulse_gen is
  generic
  (
    -- This generic enables elaboration of the fixed pulse width logic
    g_with_fixed_pwidth             : boolean;

    -- Pulse width, in number of clk_i cycles
    -- Default pulse width (20 MHz clock): 1.2 us
    -- Minimum allowable pulse width (20 MHz clock): 100 ns
    -- Maximum allowable pulse width (20 MHz clock): 2 us
    g_pwidth                        : natural range 2 to 40 := 24;

    -- Pulse period in unit of clock cycles
    g_pperiod                       : natural := 9
  );
  port
  (
    -- Clock and active-low reset inputs
    clk_i                           : in  std_logic;
    rst_n_i                         : in  std_logic;

    -- Glitch filter enable input
    -- '1' - Glitch filter disabled (glitch-sensitive, no output jitter)
    -- '0' - Glitch filter enabled (glitch-insensitive, with output jitter)
    gf_en_n_i                       : in  std_logic;

    -- Enable input, pulse generation is enabled when '1'
    en_i                            : in  std_logic;

    -- Trigger input, has to be '1' to assure pulse output with delay no greater
    -- than internal gate delays.
    trig_a_i                        : in  std_logic;
    -- Rising and falling edges of the input trigger. Externally synchronised to the clock
    trig_r_edge_p_i                 : in std_logic; --synced 1 cycle-long r edge output
    trig_f_edge_p_i                 : in std_logic; --synced 1 cycle-long f edge output

    -- Pulse error output, pulses high for one clock cycle when a pulse arrives
    -- within a pulse period
    pulse_err_p_o                   : out std_logic;

    -- Pulse output, active-high
    -- latency:
    --    glitch filter disabled: none
    --    glitch filter enabled: glitch filter length + 5 clk_i cycles
    pulse_o                         : out std_logic;
    
    -- Rising and falling edges of the output pulse, synchronised to the clock
    pulse_r_edge_p_o                : out std_logic; --synced 1 cycle-long r edge output
    pulse_f_edge_p_o                : out std_logic
  );
end entity conv_pulse_gen;


architecture behav of conv_pulse_gen is

  --============================================================================
  -- Type declarations
  --============================================================================ 
  type t_state is (
    IDLE,       -- idle state, wait for pulse
    CATCH_ERR,       -- idle state, wait for pulse
    GEN_PULSE_OUTPUT, -- pulse generation
    REJ_PULSE_INPUT -- pulse rejection

  );

  --============================================================================
  -- Constant declarations
  --============================================================================
    -- Max value of pulse counter for pulse width and pulse rejection width.
  --   Generate time:
  --        * Maximum pulse width = g_pwidth
  --        * Count starts from 0 c_max_gen = pwidth - 1 
  --        * Allow three cycle for synchrnised rising edge pwidth-4
  --        
  constant c_max_gen : natural := g_pwidth-1;
  

  --   Rejection time:
  --        * Maximum pulse period = g_pperiod*g_pwidth
  --        * Allow one cycle to change state from REJ_PULSE_INPUT to CATCH_ERR
  constant c_max_rej : natural := g_pperiod-1;
  
  -- Rising edges result from leading edge of trigger going through a 3 stage synchrnoiser.
  -- An extra 1 clock cycle delay is needed before state can be changed.
  constant c_r_edge_sync_delay : natural := 4;

  --============================================================================
  -- Function and procedure declarations
  --============================================================================
  function f_log2_size (A : natural) return natural is
  begin
    for I in 1 to 64 loop               -- Works for up to 64 bits
      if (2**I >= A) then
        return(I);
      end if;
    end loop;
    return(63);
  end function f_log2_size;

  --============================================================================
  -- Signal declarations
  --============================================================================
  -- Reset signal combining reset and en signals
  signal gen_edge_n     : std_logic;

  -- Pulse output signals
  signal pulse_out_rst_n          : std_logic; -- From FSM
  signal pulse_gf_on           : std_logic; -- Generated from synchronous input 
  signal pulse_gf_off          : std_logic; -- Generated from asynchronous input
  signal pulse_out             : std_logic; -- Selects between pulse_gf_on and pulse_gf_off

  -- Pulse length counter
  signal pulse_cnt_reset       : std_logic; -- From FSM
  signal pulse_cnt_clr       : std_logic; -- From FSM
  signal pulse_cnt             : unsigned(f_log2_size(g_pperiod)-1 downto 0);

  -- FSM states
  signal state                 : t_state;
  signal nxt_state             : t_state;

--==============================================================================
--  architecture begin
--==============================================================================
begin

gen_without_fixed_pwidth : if (g_with_fixed_pwidth = false) generate
  pulse_o       <= trig_a_i;
  pulse_err_p_o <= '0';
end generate gen_without_fixed_pwidth;

gen_with_fixed_pwidth : if (g_with_fixed_pwidth = true) generate
  -- ============================================================================
  -- Output logic
  -- ============================================================================
  
  -- pulse_out <= pulse_gf_off when (gf_en_n_i = '1') else
             -- pulse_gf_on;
             
  pulse_o <= pulse_out;
 


  -- Synchronise output to get correct rising edges and falling edges output pulses
    gen_edge_n <= rst_n_i and en_i;
    
    cmp_sync_ffs : gc_sync_ffs
    port map
    (
      clk_i    => clk_i,
      rst_n_i  => gen_edge_n,
      data_i   => pulse_out,
      ppulse_o => pulse_r_edge_p_o,
      npulse_o => pulse_f_edge_p_o
    ); 
    
  --============================================================================
  -- Pulse generation logic
  --============================================================================
  -- Generate the pulse on rising edge of trig_a_i
  
  p_pulse_gf_off : process(pulse_out_rst_n, trig_a_i, en_i)
  begin
    if pulse_out_rst_n = '0' then
      pulse_out <= '0';
    elsif rising_edge(trig_a_i) then
      if  (en_i = '1') then 
        pulse_out <= '1';
      end if;
    end if;
  end process p_pulse_gf_off;
  


  --============================================================================
  -- Pulse width adjustment logic
  --============================================================================
   p_count_cycles: process(clk_i)
  begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' or pulse_cnt_reset = '1' then
        pulse_cnt <= to_unsigned(c_r_edge_sync_delay, f_log2_size(g_pperiod));
      elsif pulse_cnt_clr = '1' then
        pulse_cnt <= (others => '0');
      else
        pulse_cnt <= pulse_cnt+1;
      end if;   
    end if;
  end process p_count_cycles; 
  
-----------------------------------------------------------------------------
-- Finite State Machine FSM
-----------------------------------------------------------------------------


-- Process to trigger state transitions
----------------------------------------
  p_fsm_transitions: process(clk_i)
  begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' then
        state                   <= IDLE;
      elsif (en_i = '1') then
        state                   <= nxt_state;
      end if;   
    end if;
  end process;
  

 -- ======================================================================================   
  -- Generate the FSM logic
  p_fsm_states : process(state, trig_r_edge_p_i, pulse_cnt)
  begin

        case state is
          ---------------------------------------------------------------------
          -- IDLE
          ---------------------------------------------------------------------
          -- Clear all values and go to pulse generation state when the
          -- appropriate input arrives
          ---------------------------------------------------------------------
          when IDLE =>
           
            if trig_r_edge_p_i = '1' then 
                nxt_state <= GEN_PULSE_OUTPUT;
            else
                nxt_state <= IDLE;
            end if;               

          when CATCH_ERR =>
            
            if pulse_cnt >= c_r_edge_sync_delay-1 then
                if trig_r_edge_p_i = '1' then
                    nxt_state <= GEN_PULSE_OUTPUT;
                else
                    nxt_state <= IDLE;
                end if;
            else
                nxt_state     <= CATCH_ERR;
            end if;
                
          ---------------------------------------------------------------------
          -- GEN_PULSE_OUTPUT
          --------------------------c-------------------------------------------
          -- Extend the generated pulse to the required pulse width.
          ---------------------------------------------------------------------
          when GEN_PULSE_OUTPUT =>
            if pulse_cnt = c_max_gen then
                nxt_state   <= REJ_PULSE_INPUT;
            else 
                nxt_state   <= GEN_PULSE_OUTPUT;
            end if;

          ---------------------------------------------------------------------
          -- REJ_PULSE_INPUT
          ---------------------------------------------------------------------
          -- Cut and reject input pulses, to safeguard the output transformers.
          ---------------------------------------------------------------------
          when REJ_PULSE_INPUT =>
            if pulse_cnt = c_max_rej then
                nxt_state   <= CATCH_ERR;
            else 
                nxt_state   <= REJ_PULSE_INPUT;
            end if;

          when others =>
            nxt_state       <= IDLE;

        end case;

  end process p_fsm_states;

  
    -- Generate the FSM logic
  p_fsm_outputs : process(state, trig_r_edge_p_i, pulse_cnt, rst_n_i)
  begin

        case state is
          ---------------------------------------------------------------------
          -- IDLE
          ---------------------------------------------------------------------
          -- Clear all values and go to pulse generation state when the
          -- appropriate input arrives
          ---------------------------------------------------------------------
          when IDLE =>
            pulse_out_rst_n <= rst_n_i;
            pulse_err_p_o   <= '0';
            pulse_cnt_reset <= '1';
            pulse_cnt_clr   <= '0';

          when CATCH_ERR =>
            pulse_out_rst_n <= rst_n_i;
            if pulse_cnt < c_r_edge_sync_delay-1 then
                    pulse_err_p_o   <= trig_r_edge_p_i;
            else
                    pulse_err_p_o   <= '0';
            end if;
            
            pulse_cnt_reset <= '0';
            pulse_cnt_clr   <= '0';
            
          ---------------------------------------------------------------------
          -- GEN_PULSE_OUTPUT
          --------------------------c-------------------------------------------
          -- Extend the generated pulse to the required pulse width.
          ---------------------------------------------------------------------
          when GEN_PULSE_OUTPUT =>
            pulse_out_rst_n <= rst_n_i;
            pulse_err_p_o   <= trig_r_edge_p_i;
            pulse_cnt_reset <= '0';
            pulse_cnt_clr   <= '0';

          ---------------------------------------------------------------------
          -- REJ_PULSE_INPUT
          ---------------------------------------------------------------------
          -- Cut and reject input pulses, to safeguard the output transformers.
          ---------------------------------------------------------------------
          when REJ_PULSE_INPUT =>
                pulse_err_p_o   <= trig_r_edge_p_i;
                pulse_out_rst_n <= '0';
                pulse_cnt_reset <= '0';
            if pulse_cnt = c_max_rej then
                pulse_cnt_clr   <= '1';
            else 
                pulse_cnt_clr   <= '0';
            end if;
            

          when others =>
            pulse_out_rst_n     <= '0';
            pulse_err_p_o       <= '0';
            pulse_cnt_reset     <= '1';


        end case;

  end process p_fsm_outputs;
  
  
end generate gen_with_fixed_pwidth;

end architecture behav;
--==============================================================================
--  architecture end
--==============================================================================
