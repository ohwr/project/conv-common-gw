
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.gencores_pkg.all;
use work.wishbone_pkg.all;

use work.conv_common_gw_pkg.all;



entity wf_decr_counter is
  generic(g_counter_lgth : natural := 4);                         -- default length
  port(
  -- INPUTS
    -- nanoFIP User Interface general signal
    uclk_i            : in std_logic;                             -- 40 MHz clock

    -- Signal from the wf_reset_unit
    counter_rst_i     : in std_logic;                             -- resets counter to all '1'

    -- Signals from any unit
    counter_decr_i    : in std_logic;                             -- decrement enable
    counter_load_i    : in std_logic;                             -- load enable; loads counter to counter_top_i
    counter_top_i     : in unsigned (g_counter_lgth-1 downto 0);  -- load value


  -- OUTPUTS
    -- Signal to any unit
    counter_o         : out unsigned (g_counter_lgth-1 downto 0); -- counter
    counter_is_zero_o : out std_logic);                           -- empty counter indication

end entity wf_decr_counter;


--=================================================================================================
--                                    architecture declaration
--=================================================================================================
architecture rtl of wf_decr_counter is

  signal s_counter : unsigned (g_counter_lgth-1 downto 0);


--=================================================================================================
--                                       architecture begin
--=================================================================================================
begin


---------------------------------------------------------------------------------------------------
  -- Synchronous process Decr_Counter

  Decr_Counter: process (uclk_i)
  begin
    if rising_edge (uclk_i) then

      if counter_rst_i = '1' then
        s_counter   <= (others => '1');
      else

        if counter_load_i = '1' then
          s_counter <= counter_top_i;

        elsif counter_decr_i = '1' then
          s_counter <= s_counter - 1;

        end if;
      end if;
    end if;
  end process;

--  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --

  counter_o         <= s_counter;
  counter_is_zero_o <= '1' when s_counter = to_unsigned(0, s_counter'length) else '0';
  
  end architecture ;
